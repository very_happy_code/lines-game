﻿#nullable enable

using global::System;
using Microsoft.VisualStudio.TestTools.UnitTesting;


namespace UnitTests
{
    [type: TestClass]
    public class c_ReflectionAttributesTests
    {
        [method: TestMethod("Test types on IndexationEnumAttribute condition")]
        public void TestTypesOnIndexationEnumAttributeCondition()
        {
            var ex = LinesGame.sc_ModuleInitializer.st_TestTypesOnIndexationEnumAttributeCondition();
            Assert.IsNull(ex, ex?.Message);
        }

        [method: TestMethod("Test types on FactoryUnitAttribute condition")]
        public void TestTypesOnFactoryUnitAttributeCondition()
        {
            var ex = LinesGame.sc_ModuleInitializer.st_TestTypesOnFactoryUnityAttributeCondition();
            Assert.IsNull(ex, ex?.Message);
        }
    }
}