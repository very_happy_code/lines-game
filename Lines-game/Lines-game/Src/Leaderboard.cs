﻿#nullable enable

using global::System;
using System.IO;
using System.Collections.Generic;
using System.Runtime.Serialization.Formatters.Binary;
using System.Collections;

using static LinesGame.sc_Debug;

using ros_LeaderboardEntry_t = LinesGame.c_Leaderboard.ros_LeaderboardEntry;


namespace LinesGame
{
    internal class c_Leaderboard : I_EnumerableRef<ros_LeaderboardEntry_t>
    {
        #region Types
        [type: Serializable]
        public readonly struct ros_LeaderboardEntry
        {
            public readonly string m_ro_sUserName;
            public readonly uint m_ro_unScore;
            public readonly float m_ro_fPlayTimeSeconds;
            public readonly string m_ro_sDate;

            public ros_LeaderboardEntry(string userName, uint score, float playTimeSec, string date)
            {
                this.m_ro_sUserName = userName;
                this.m_ro_unScore = score;
                this.m_ro_fPlayTimeSeconds = playTimeSec;
                this.m_ro_sDate = date;
            }
        }

        [type: Serializable]
        private record r_leaderboardFile (LinkedList<ros_LeaderboardEntry> pr_Entries, string pr_sGameVersion);

        private struct s_leaderboardIterator : I_EnumeratorRef<ros_LeaderboardEntry>
        {
            private readonly LinkedList<ros_LeaderboardEntry> ro_list;
            private LinkedListNode<ros_LeaderboardEntry>? current;


            public readonly ref readonly ros_LeaderboardEntry Current => ref current!.ValueRef;

            readonly ros_LeaderboardEntry IEnumerator<ros_LeaderboardEntry>.Current => current!.Value;
            readonly object IEnumerator.Current => current!.Value;


            public readonly void Dispose() { /* noop */ }
            public bool MoveNext()
            {
                if (current != null) {
                    current = current.Next;
                }
                else {
                    current = ro_list.First;
                }
                return current != null;
            }
            public void Reset()
            {
                current = null;
            }


            public s_leaderboardIterator(LinkedList<ros_LeaderboardEntry> list)
            {
                this.ro_list = list;
                this.current = null;
            }
        }
        #endregion


        private const int nMAX_ENTRIES = 30;


        private LinkedList<ros_LeaderboardEntry> entries;


        public int pr_nEntriesNumber { get => entries.Count; }


        public void SaveOnDisk(out string? errorMessage)
        {
            var error = sc_Utils.st_OpenStreamToWrite(sc_LinesDirectoriesHub.sLEADERBOARD_DIR, sc_LinesDirectoriesHub.sLEADERBOARD_FILE, (FileStream stream) => {
                r_leaderboardFile file = new(entries, sc_Utils.st_GetGameVersion());
                BinaryFormatter formatter = new();

#pragma warning disable SYSLIB0011 // Type or member is obsolete
                formatter.Serialize(stream, file);
#pragma warning restore SYSLIB0011 // Type or member is obsolete
            });

            errorMessage = error?.Message ?? null;
        }
        public void AddEntry(in ros_LeaderboardEntry entry)
        {
            if (entries.Count == 0) {
                entries.AddFirst(entry);
                return;
            }
            else if (entries.Count == nMAX_ENTRIES) {
                entries.RemoveLast();
            }

            var curr = entries.First!;

            while (entry.m_ro_unScore < curr!.ValueRef.m_ro_unScore) {
                curr = curr.Next;
                if (curr == null) { //reach the end of the list., new entry should be added in the end
                    break;
                }
            }

            if (curr is not null)
                entries.AddBefore(curr, entry);
            else
                entries.AddLast(entry);
        }
        public ref readonly ros_LeaderboardEntry GetEntryRRef(int idx)
        {
            st_Assert(entries.Count > 0, "List is empty!", new InvalidOperationException());
            st_Assert(idx >= 0 && idx < entries.Count, $"idx = {idx}, count = {entries.Count}", new IndexOutOfRangeException());

            var current = entries.First!;

            while (idx > 0) {
                current = current.Next!;
            }
            return ref current.ValueRef;
        }


        private void loadEntriesFromDisk(out bool result)
        {
            var exception = sc_Utils.st_OpenStreamToRead(sc_LinesDirectoriesHub.sLEADERBOARD_DIR, sc_LinesDirectoriesHub.sLEADERBOARD_FILE, delegate (FileStream stream) {
                BinaryFormatter formatter = new();

#pragma warning disable SYSLIB0011 // Type or member is obsolete
                r_leaderboardFile file = (formatter.Deserialize(stream) as r_leaderboardFile)!;
#pragma warning restore SYSLIB0011 // Type or member is obsolete

                if (!file.pr_sGameVersion?.Equals(sc_Utils.st_GetGameVersion()) ?? true) {
                    throw new Exception($"File version inconsistency! File has version \"{file.pr_sGameVersion}\", game have version: {sc_Utils.st_GetGameVersion()}");
                }

                entries = file.pr_Entries;

                if (entries.Count > nMAX_ENTRIES)
                    throw new Exception("Too much entries in file! Num: " + entries.Count);
            });

            if (exception != null)
                sc_Logger.st_WriteLog(exception, "Can't read leaderboard", null);

            result = exception == null;
        }


        #region I_Enumerator
        public I_EnumeratorRef<ros_LeaderboardEntry_t> GetEnumerator()
        {
            return new s_leaderboardIterator(entries);
        }
        IEnumerator<ros_LeaderboardEntry_t> IEnumerable<ros_LeaderboardEntry_t>.GetEnumerator()
        {
            return new s_leaderboardIterator(entries);
        }
        IEnumerator IEnumerable.GetEnumerator()
        {
            return new s_leaderboardIterator(entries);
        }
        #endregion


        public c_Leaderboard()
        {
            this.entries = new LinkedList<ros_LeaderboardEntry>();

#if DEBUG
            loadEntriesFromDisk(out bool result);

            if (!result)
                AddEntry(new ros_LeaderboardEntry("Player 1", 156, 421, "01.01.2021"));
#else
            loadEntriesFromDisk(out _);
#endif
        }
    }
}