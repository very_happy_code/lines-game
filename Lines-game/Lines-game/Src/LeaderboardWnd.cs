﻿#nullable enable

using global::System;
using System.Windows.Forms;


namespace LinesGame
{
    internal partial class c_LeaderboardWnd : Form
    {
        private readonly c_Leaderboard ro_leaderboard;


        public c_LeaderboardWnd(c_Leaderboard leaderboard)
        {
            InitializeComponent();

            this.ro_leaderboard = leaderboard;
            displayEntries();
        }


        private void displayEntries()
        {
            listView.Items.Clear(); //clear existing entries

            foreach (ref readonly var entry in ro_leaderboard) {
                ListViewItem item = new(entry.m_ro_sUserName);
                item.SubItems.Add(entry.m_ro_unScore.ToString());
                TimeSpan ts = new(0, 0, ( int )entry.m_ro_fPlayTimeSeconds);
                item.SubItems.Add(ts.ToString(@"hh\:mm\:ss"));
                item.SubItems.Add(entry.m_ro_sDate);

                listView.Items.Add(item);
            }
        }
    }
}
