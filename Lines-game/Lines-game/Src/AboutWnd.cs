﻿#nullable enable

using global::System;
using System.Reflection;
using System.Windows.Forms;


namespace LinesGame
{
    internal partial class c_AboutWnd : Form
    {
        public c_AboutWnd()
        {
            InitializeComponent();
            this.Text = String.Format("About {0}", st_pr_AssemblyTitle);
            this.labelProductName.Text = st_pr_AssemblyProduct;
            this.labelVersion.Text = String.Format("Version {0}", st_pr_AssemblyVersion);
            this.labelCopyright.Text = st_pr_AssemblyCopyright;
            this.labelCompanyName.Text = st_pr_AssemblyCompany;
            this.textBoxDescription.Text = st_pr_AssemblyDescription;
        }

        #region Assembly Attribute Accessors

        public static string st_pr_AssemblyTitle
        {
            get {
                object[] attributes = Assembly.GetExecutingAssembly().GetCustomAttributes(typeof(AssemblyTitleAttribute), false);
                if (attributes.Length > 0) {
                    AssemblyTitleAttribute titleAttribute = ( AssemblyTitleAttribute )attributes[0];
                    if (titleAttribute.Title != "") {
                        return titleAttribute.Title;
                    }
                }
                /*
                 * Can't use Assembly.Location because of the build needs:
                 * 'System.Reflection.Assembly.Location' always returns an empty string for assemblies embedded in a single-file app. If the path to the app directory is needed, consider calling 'System.AppContext.BaseDirectory'.
                 */
                return System.IO.Path.GetFileNameWithoutExtension(AppContext.BaseDirectory/*Assembly.GetExecutingAssembly().Location*/);
            }
        }

        public static string st_pr_AssemblyVersion
        {
            get {
                var attrs = (Assembly.GetExecutingAssembly().GetCustomAttributes(typeof(AssemblyInformationalVersionAttribute), true) as AssemblyInformationalVersionAttribute[])!;
                if ((attrs?.Length ?? 0) != 0) {
                    return attrs![0].InformationalVersion;
                }
                else {
                    return Assembly.GetExecutingAssembly().GetName().Version?.ToString() ?? "---";
                }
            }
        }

        public static string st_pr_AssemblyDescription
        {
            get {
                object[] attributes = Assembly.GetExecutingAssembly().GetCustomAttributes(typeof(AssemblyDescriptionAttribute), false);
                if (attributes.Length == 0) {
                    return "---";
                }
                return (( AssemblyDescriptionAttribute )attributes[0]).Description;
            }
        }

        public static string st_pr_AssemblyProduct
        {
            get {
                object[] attributes = Assembly.GetExecutingAssembly().GetCustomAttributes(typeof(AssemblyProductAttribute), false);
                if (attributes.Length == 0) {
                    return "---";
                }
                return (( AssemblyProductAttribute )attributes[0]).Product;
            }
        }

        public static string st_pr_AssemblyCopyright
        {
            get {
                object[] attributes = Assembly.GetExecutingAssembly().GetCustomAttributes(typeof(AssemblyCopyrightAttribute), false);
                if (attributes.Length == 0) {
                    return "---";
                }
                return (( AssemblyCopyrightAttribute )attributes[0]).Copyright;
            }
        }

        public static string st_pr_AssemblyCompany
        {
            get {
                object[] attributes = Assembly.GetExecutingAssembly().GetCustomAttributes(typeof(AssemblyCompanyAttribute), false);
                if (attributes.Length == 0) {
                    return "---";
                }
                return (( AssemblyCompanyAttribute )attributes[0]).Company;
            }
        }
        #endregion
    }
}
