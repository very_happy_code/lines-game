﻿#nullable enable

using global::System;
using System.Collections.Generic;
using System.Threading;

using static LinesGame.sc_Debug;


namespace LinesGame
{
    internal sealed partial record ser_Coroutine
    {
        internal static class sc_CoroutineManager
        {
            private record r_entry(ser_Coroutine pr_Handle)
            {
                public en_CoroutineBehavior? m_LastAnswerType = null;

                /* Save state for WaitForSeconds behavior: */
                public float m_fWaitForSecondsRemain = .0f;
                public float m_fWaitForSecondsInitial = .0f;
            }
            //==============================================================================================================
            private const uint unTIMER_PERIOD_MILLISECONDS = 15;

            private static readonly List<r_entry> activeCoroutines = new();
            private static uint unNextID = 0U;
            private static Timer? updateTimer = null;


            public static ser_Coroutine st_StartCoroutine(dl_CoroutineFunc func)
            {
                return st_StartCoroutine(func.Invoke());
            }
            public static ser_Coroutine st_StartCoroutine(IEnumerable<I_CoroutineBehavior> coroutine)
            {
                ser_Coroutine handle = new(coroutine.GetEnumerator()) { pr_unID = unNextID++, _bIsExecuting = true };
                activeCoroutines.Add(new r_entry(handle));
                return handle;
            }
            public static void st_RestartCoroutine(ser_Coroutine handle)
            {
                const string EXECUTING = "Function already executing!";
                st_Assert(!handle._bIsExecuting, EXECUTING, new InvalidOperationException(EXECUTING));

                handle.ro_activeIterator.Reset();
                activeCoroutines.Add(new r_entry(handle));
            }

            public static void st_StopCoroutine(ser_Coroutine handle)
            {
                const string NOT_EXECUTING = "Function does not executing!";
                st_Assert(handle._bIsExecuting, NOT_EXECUTING, new InvalidOperationException(NOT_EXECUTING));

                int idx = activeCoroutines.FindIndex((ent) => ent.pr_Handle.pr_unID == handle.pr_unID);

                if (idx != -1) {
                    //activeCoroutines[idx].pr_Handle.ro_activeIterator.Dispose();
                    activeCoroutines.RemoveAt(idx);
                    handle._bIsExecuting = false;
                }
                else
                    throw new InvalidOperationException(NOT_EXECUTING);
            }
            public static void st_StopAllCoroutines()
            {
                for (int i = 0; i < activeCoroutines.Count; ++i) {
                    //activeCoroutines[i].pr_Handle.ro_activeIterator.Dispose();
                    activeCoroutines[i].pr_Handle._bIsExecuting = false;
                }
                activeCoroutines.Clear();
            }

            internal static void st_Update(float dtSeconds)
            {
                for (int i = 0; i < activeCoroutines.Count; ++i) {
                    var entry = activeCoroutines[i];

                    if (entry.m_LastAnswerType != null) {
                        switch (entry.m_LastAnswerType) {
                            case en_CoroutineBehavior.eWaitForNextFrame: break; //execute frame

                            case en_CoroutineBehavior.eWaitForSeconds:
                                entry.m_fWaitForSecondsRemain -= dtSeconds;

                                if (entry.m_fWaitForSecondsRemain <= .0f) {
                                    break; //execute frame
                                }
                                else continue;  //skip execution

#if DEBUG
                            default:
                                throw new Exception("Unknown operation type: " + entry.m_LastAnswerType.ToString());
#endif
                        }
                    }

                    //execute current frame:
                    bool advance = entry.pr_Handle.ro_activeIterator.MoveNext();

                    if (advance) {
                        var answer = entry.pr_Handle.ro_activeIterator.Current;
                        entry.m_LastAnswerType = answer.pr_IntendedBehavior;

                        if (entry.m_LastAnswerType == en_CoroutineBehavior.eWaitForSeconds) {
                            float delay = (( s_CoroutineWaitForSeconds )answer).pr_fDelaySeconds;
                            entry.m_fWaitForSecondsInitial = entry.m_fWaitForSecondsRemain = delay;
                        }
                    }
                    else {
                        activeCoroutines.RemoveAt(i--);
                        entry.pr_Handle._bIsExecuting = false;
                    }
                }
            }

            internal static void st_StartUpdatingViaThreadPool()   //run updates in separate thread
            {
                st_Assert(updateTimer == null, "Update already started!", new InvalidOperationException());

                const float PERIOD_SECONDS = unTIMER_PERIOD_MILLISECONDS / 1000.0f;

                updateTimer = new Timer( static (_) => st_Update(PERIOD_SECONDS), state: null, dueTime: 0, period: ( int )unTIMER_PERIOD_MILLISECONDS);
            }
            internal static void st_StopUpdating()
            {
                st_Assert(updateTimer != null, "Updtae is not working!", new InvalidOperationException());

                updateTimer!.Dispose();
                updateTimer = null;
            }
        }
    }
}