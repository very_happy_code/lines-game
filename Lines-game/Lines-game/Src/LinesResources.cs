﻿#nullable enable

using global::System;
using System.Drawing;
using System.Media;

using static LinesGame.sc_Debug;

using r_SpriteSheetParams_t = LinesGame.c_GDISpriteSheetResource.r_SpriteSheetParams;
using r_SpriteAnimationParams_t = LinesGame.c_GDISpriteAnimationResource.r_SpriteAnimationParams;
using r_LinesAnimationParams_t = LinesGame.c_LinesAnimationResource.r_LinesAnimationParams;
using r_ImageFontParams_t = LinesGame.c_GDIImageFontResource.r_ImageFontParams;


namespace LinesGame
{
    internal class c_LinesAnimationResource : c_GDISpriteAnimationResource, ac_Resource.I_ParameterizedSourceLoadable<c_GDISpriteSheetResource, r_LinesAnimationParams_t>
    {
        public record r_LinesAnimationParams (en_BallAnimations pr_AnimType, float pr_fLengthSeconds, int pr_nAnimSourceIdx_Y, int pr_nAnimSourceIdx_X, int pr_nAnimLengthFrames )
            : r_SpriteAnimationParams_t (pr_fLengthSeconds, pr_nAnimSourceIdx_Y, pr_nAnimSourceIdx_X, pr_nAnimLengthFrames);
        //--------------------------------------------------------------------------------------------------------------
        public en_BallAnimations pr_AnimType { get; private set; }


        void I_ParameterizedSourceLoadable<c_GDISpriteSheetResource, r_LinesAnimationParams_t>.loadFromSourceWithParams(c_GDISpriteSheetResource source, r_LinesAnimationParams_t @params)
        {
            pr_AnimType = @params.pr_AnimType;
            base.loadResourceFromSourceWithParams(source, @params);
        }
    }


    internal static class sc_LinesResourcesProvider
    {
        //coming from resource:
        public const float fFIELD_BORDER_OFFSET_PX = 3.0f;
        public const float fFIELD_CELL_SIZE_PX = 44.0f;
        public const float fGAP_BETWEEN_CELLS_PX = 1.0f;
        public const char cTIME_DELEMITTER = ':';

        private const string sFIELD_RESOURCE_NAME = "field_background.png";
        private const string sPANEL_RESOURCE_NAME = "panel_background.png";
        private const string sHINT_SHEET_RESOURCE_NAME = "hint.png";
        private const string sANIMS_SHEET_RESOURCE_NAME = "balls.png";
        private const string sSCORE_SHEET_RESOURCE_NAME = "score.png";
        private const string sSCORE_FONT_RESOURCE_NAME = "font_score";  //virtual resource (I mean, created from another resource, not taken from disk)
        private const string sTIME_SHEET_RESOURCE_NAME = "time.png";
        private const string sCOLON_RESOURCE_NAME = "colon_char.png";
        private const string sTIME_FONT_RESOURCE_NAME = "font_time";    //virtual resource
        private const string sSOUND_MOVE_RESOURCE_NAME = "move.wav";
        private const string sSOUND_NO_MOVE_RESOURCE_NAME = "no_move.wav";
        private const string sSOUND_SELECT_RESOURCE_NAME = "choose.wav";
        private const string sSOUND_DESTROY_RESOURCE_NAME = "disappear.wav";
        private const string sSOUND_GAME_OVER_RESOURCE_NAME = "over.wav";
        private const string sSOUND_NOTIFICATION_RESOURCE_NAME = "notification.wav";

        private static readonly c_LinesAnimationResource?[,] ro_animationsCache;


        public static c_GDIImageResource st_GetFieldBackground()
        {
            return c_ResourceManager.st_pr_Instance.LoadResourceFromAssembly<c_GDIImageResource>(sFIELD_RESOURCE_NAME);
        }
        public static c_GDIImageResource st_GetPanelBackground()
        {
            return c_ResourceManager.st_pr_Instance.LoadResourceFromAssembly<c_GDIImageResource>(sPANEL_RESOURCE_NAME);
        }
        public static Image st_GetHintImage(en_BallColors color)
        {
            const int HINT_IMAGES_NUMBER = 7;  //comes from resource image
        
            var rm = c_ResourceManager.st_pr_Instance;
            var sheet = rm.GetResource<c_GDISpriteSheetResource>(sHINT_SHEET_RESOURCE_NAME);

            if (sheet == null) {
                sheet = rm.LoadResourceFromAssembly<c_GDISpriteSheetResource, r_SpriteSheetParams_t>(
                    name: sHINT_SHEET_RESOURCE_NAME,
                    @params: new r_SpriteSheetParams_t(HINT_IMAGES_NUMBER, pr_nMapHeightSprites: 1));
            }

            int image_idx = getIdexOfColorInResource(color);

            return sheet[image_idx, y: 0];
        }

        public static c_LinesAnimationResource st_GetBallAnimationResource(en_BallAnimations anim, en_BallColors color)
        {
            //comes from image:
            const int SHEET_WIDTH_SPRITES = 22;
            const int SHEET_HEIGHT_SPRITES = 7;

            var resource = ro_animationsCache[( int )anim, ( int )color];

            //if we have resource in local cache:
            if (resource != null) {
                st_Assert(resource.pr_bIsLoaded, "Resource is not loaded!");
                return resource;
            }

            //if not, try to check resource manager:
            string target_res_name = $"ball_anim_{anim}_{color}";

            var rm = c_ResourceManager.st_pr_Instance;
            resource = rm.GetResource<c_LinesAnimationResource>(target_res_name);

            //if resource manager also don't have a resource:
            if (resource == null) {
                //get spritesheet for animations:
                var sheet = rm.LoadResourceFromAssembly<c_GDISpriteSheetResource, r_SpriteSheetParams_t>(
                        name: sANIMS_SHEET_RESOURCE_NAME,
                        @params: new r_SpriteSheetParams_t(SHEET_WIDTH_SPRITES, SHEET_HEIGHT_SPRITES, pr_TransparencyMask: Color.FromArgb(192, 192, 192))   //mask is coming from resource
                    );


                /* create anim from sheet: */

                int source_idx_y = getIdexOfColorInResource(color);
                int source_idx_x, length_frames;
                float length_sec;

                //values comes from resource:
                switch (anim) {
                    default:
                        st_Assert(false, "Invalid anim: " + anim.ToString());
                        goto case en_BallAnimations.eBallJumping;

                    case en_BallAnimations.eBallJumping:
                        source_idx_x = 0;
                        length_frames = 7;
                        length_sec = 0.7f;
                        break;
                    case en_BallAnimations.eBallDestructing:
                        source_idx_x = 7;
                        length_frames = 10;
                        length_sec = 0.5f;
                        break;
                    case en_BallAnimations.eBallGrowing:
                        source_idx_x = 17;
                        length_frames = 5;
                        length_sec = 0.3f;
                        break;
                }

                resource = rm.LoadResourceFromSource<c_LinesAnimationResource, c_GDISpriteSheetResource, r_LinesAnimationParams_t>(
                        name: target_res_name,
                        source: sheet,
                        @params: new r_LinesAnimationParams_t(anim, length_sec, source_idx_y, source_idx_x, length_frames)
                    );
            }

            ro_animationsCache[( int )anim, ( int )color] = resource;   //cahce the resource
            return resource;
        }
        public static s_Animation st_GetNewBallAnimation(en_BallAnimations anim, en_BallColors color)
        {
            var res = st_GetBallAnimationResource(anim, color);
            var animation = new s_Animation() { pr_Resource = res };

            //comes from resource:
            animation.m_bWrapTime = anim == en_BallAnimations.eBallJumping;
            animation.m_bInverseFrames = anim == en_BallAnimations.eBallGrowing;

            return animation;
        }
        public static Image st_GetBallImage(en_BallColors color, bool isGrowed)
        {
            //take full image from animation frames (this comes from resource):
            if (isGrowed)
                return st_GetBallAnimationResource(en_BallAnimations.eBallGrowing, color)[0];
            else
                return st_GetBallAnimationResource(en_BallAnimations.eBallGrowing, color)[^1];
        }

        public static c_GDIImageFontResource st_GetScoreFont()
        {
            var res = c_ResourceManager.st_pr_Instance.GetResource<c_GDIImageFontResource>(sSCORE_FONT_RESOURCE_NAME);

            if (res != null) {
                return res;
            }
            else {
                var rm = c_ResourceManager.st_pr_Instance;
                var sheet = rm.LoadResourceFromAssembly<c_GDISpriteSheetResource, r_SpriteSheetParams_t>(sSCORE_SHEET_RESOURCE_NAME, new r_SpriteSheetParams_t(10, 1));
                char[] chars = new char[] { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9' };
                var font = c_ResourceManager.st_pr_Instance.LoadResourceFromSource<c_GDIImageFontResource, c_GDISpriteSheetResource, char[]>(sSCORE_FONT_RESOURCE_NAME, sheet, chars);

                rm.FreeResource(sSCORE_SHEET_RESOURCE_NAME);    //we don't need this anymore (used only for create font resource)
                return font;
            }
        }
        public static c_GDIImageFontResource st_GetTimeFont()
        {
            var res = c_ResourceManager.st_pr_Instance.GetResource<c_GDIImageFontResource>(sTIME_FONT_RESOURCE_NAME);

            if (res is not null) {
                return res;
            }
            else {
                var rm = c_ResourceManager.st_pr_Instance;

                var sheet = rm.LoadResourceFromAssembly<c_GDISpriteSheetResource, r_SpriteSheetParams_t>(sTIME_SHEET_RESOURCE_NAME, new r_SpriteSheetParams_t(10, 1));
                var additional_images = new Image[] { rm.LoadResourceFromAssembly<c_GDIImageResource>(sCOLON_RESOURCE_NAME).pr_Image! };
                var chars = new char[] { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9' };
                r_ImageFontParams_t @params = new(chars, additional_images, new char[] { cTIME_DELEMITTER });
                var font = c_ResourceManager.st_pr_Instance.LoadResourceFromSource<c_GDIImageFontResource, c_GDISpriteSheetResource, r_ImageFontParams_t>(sTIME_FONT_RESOURCE_NAME, sheet, @params);

                rm.FreeResource(sTIME_SHEET_RESOURCE_NAME);    //we don't need this anymore (used only for create font resource)
                rm.FreeResource(sCOLON_RESOURCE_NAME);
                return font;
            }
        }

        public static c_StreamResource st_GetSoundResource(en_LinesSounds sound)
        {
#if !DEBUG
#pragma warning disable CS8524
#endif
            string res_name = sound switch {
                en_LinesSounds.eSelect => sSOUND_SELECT_RESOURCE_NAME,
                en_LinesSounds.eMove => sSOUND_MOVE_RESOURCE_NAME,
                en_LinesSounds.eNoMove => sSOUND_NO_MOVE_RESOURCE_NAME,
                en_LinesSounds.eDestroy => sSOUND_DESTROY_RESOURCE_NAME,
                en_LinesSounds.eGameOver => sSOUND_GAME_OVER_RESOURCE_NAME,
                en_LinesSounds.eNotification => sSOUND_NOTIFICATION_RESOURCE_NAME,
#if DEBUG
                _ => throw new ArgumentException($"Unexpected value of {nameof(sound)} = {sound}")
#endif
            };
#if !DEBUG
#pragma warning restore CS8524
#endif

            return c_ResourceManager.st_pr_Instance.LoadResourceFromAssembly<c_StreamResource>(res_name, tryGet: true);
        }


        //values comes from images:
        private static int getIdexOfColorInResource(en_BallColors color)
        {
#if !DEBUG
#pragma warning disable CS8524 // The switch expression does not handle some values of its input type (it is not exhaustive) involving an unnamed enum value.
#endif
            return color switch {
                en_BallColors.eLightRed     => 0,
                en_BallColors.eDarkRed      => 1,
                en_BallColors.eDarkBlue     => 2,
                en_BallColors.eLightBlue    => 3,
                en_BallColors.eGreen        => 4,
                en_BallColors.ePink         => 5,
                en_BallColors.eYellow       => 6,
#if DEBUG
                _ => throw new Exception()
#endif
            };
#if !DEBUG
#pragma warning restore CS8524 // The switch expression does not handle some values of its input type (it is not exhaustive) involving an unnamed enum value.
#endif
        }


        static sc_LinesResourcesProvider()
        {
            int anims_number = sc_Utils.st_GetEnumValuesNum<en_BallAnimations>();
            int colors_number = sc_Utils.st_GetEnumValuesNum<en_BallColors>();
            ro_animationsCache = new c_LinesAnimationResource[anims_number, colors_number];
        }
    }


    internal static class sc_LinesSounds
    {
        private static readonly SoundPlayer?[] ro_players;


        public static SoundPlayer st_GetSound(en_LinesSounds sound)
        {
            var player = ro_players[( int )sound];

            if (player == null) {
                var res = sc_LinesResourcesProvider.st_GetSoundResource(sound);
                player = new SoundPlayer(res.pr_Stream!);
                player.Load();
                ro_players[( int )sound] = player;
            }

            return player;
        }


        static sc_LinesSounds()
        {
            ro_players = new SoundPlayer[sc_Utils.st_GetEnumValuesNum<en_LinesSounds>()];
        }
    }
}