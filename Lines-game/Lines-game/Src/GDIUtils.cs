﻿#nullable enable

using global::System;
using System.Drawing;
using System.Drawing.Imaging;
using System.Runtime.InteropServices;


using static LinesGame.sc_Debug;


namespace LinesGame
{
    internal static class sc_GDIUtils
    {
        [type: StructLayout(LayoutKind.Sequential)]
        private ref struct rs_color_argb32
        {
            public byte m_byB;
            public byte m_byG;
            public byte m_byR;
            public byte m_byA;

            public readonly override string ToString()
            {
                return $"(R:{m_byR},G:{m_byG},B:{m_byB},A:{m_byA})";
            }
        }


        public static unsafe void st_ApplyTransparencyMask(Bitmap bmp, Color mask)
        {
            const byte TRANSPARENT = 0;

            st_Assert(bmp.PixelFormat == PixelFormat.Format32bppArgb, "Invalid bitmap pixel format: " + bmp.PixelFormat.ToString(), new ArgumentException(nameof(bmp)));

            var data = bmp.LockBits(Rectangle.FromLTRB(0, 0, bmp.Width, bmp.Height), ImageLockMode.ReadWrite, bmp.PixelFormat);

            rs_color_argb32* color_ptr = ( rs_color_argb32* )data.Scan0;

            int size = data.Width * data.Height;

            for (int i = 0; i < size; ++i) {
                if (color_ptr->m_byR == mask.R &&
                    color_ptr->m_byG == mask.G &&
                    color_ptr->m_byB == mask.B) {
                    color_ptr->m_byA = TRANSPARENT;
                }
                color_ptr++;
            }

            bmp.UnlockBits(data);
        }
    }
}