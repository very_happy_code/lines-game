﻿#nullable enable

using global::System;

using static LinesGame.sc_Debug;


namespace LinesGame
{
    internal struct s_BallPosition
    {
        public int m_nX, m_nY;

        public readonly void Deconstruct(out int x, out int y) => (x, y) = (m_nX, m_nY);

        public readonly bool Equals((int x, int y) tuple) => (m_nX, m_nY) == tuple;
    }


    [type: atr_IndexationEnum]
    internal enum en_BallColors : byte
    {
        eDarkBlue = 0x00,
        eLightBlue,
        eDarkRed,
        eLightRed,
        eGreen,
        eYellow,
        ePink
    }


    internal enum en_BallState : byte
    {
        eNotGrowed = 0x00,
        eGrowing,
        eGrowed,
        eDestructing
    }


    [type: atr_IndexationEnum]
    internal enum en_BallAnimations : byte
    {
        eBallJumping = 0x00,
        eBallDestructing,
        eBallGrowing,
    }


    internal class c_Ball
    {
        public s_Animation? m_CurrentAnim = null;

        private en_BallState _state;
        private en_BallColors _color;

        public en_BallColors pr_Color { get => _color; init { _color = value; } }
        public en_BallState pr_State { get => _state; }

        public void StartGrowing()
        {
            st_Assert(_state == en_BallState.eNotGrowed, "Invalid state: " + _state.ToString(), new InvalidOperationException());

            _state = en_BallState.eGrowing;
            m_CurrentAnim = sc_LinesResourcesProvider.st_GetNewBallAnimation(en_BallAnimations.eBallGrowing, _color);
        }
        public void Grow()
        {
            st_Assert(_state == en_BallState.eGrowing, "Invalid state: " + _state.ToString(), new InvalidOperationException());

            _state = en_BallState.eGrowed;
            m_CurrentAnim = null;
        }
        public void Destroy()
        {
            st_Assert(_state == en_BallState.eGrowed || _state == en_BallState.eDestructing, "Invalid state: " + _state.ToString(), new InvalidOperationException());

            _state = en_BallState.eDestructing;
            m_CurrentAnim = sc_LinesResourcesProvider.st_GetNewBallAnimation(en_BallAnimations.eBallDestructing, _color);
        }
        public void UpdateAnim(float dtSeconds)
        {
            if (m_CurrentAnim != null) {
                var anim = m_CurrentAnim.Value;
                anim.pr_fCurrentTimeSeconds += dtSeconds;
                m_CurrentAnim = anim;
            }
        }

        public c_Ball(en_BallColors color, en_BallState state)
        {
            this._color   = color;
            this._state   = state;
        }
    }


    internal class c_FreeBall : c_Ball
    {
        public s_BallPosition m_Position;


        public c_FreeBall(s_BallPosition pos, en_BallColors color, en_BallState growState)
            : base(color, growState)
        {
            this.m_Position = pos;
        }
        public c_FreeBall(s_BallPosition pos, c_Ball stationaryBall)
            : this(pos, stationaryBall.pr_Color, stationaryBall.pr_State)
        {
            m_CurrentAnim = stationaryBall.m_CurrentAnim;
        }
    }


    //Data Transfer Objects (logic->UI):
    internal record r_BallDTO(en_BallColors Color, en_BallState State, s_Animation? CurrentAnim);
    internal record r_MovingBallDTO(s_BallPosition Pos, en_BallColors Color, en_BallState State, s_Animation? CurrentAnim) : r_BallDTO(Color, State, CurrentAnim);
}