#nullable enable

using global::System;
using System.Windows.Forms;
#if !DEBUG
using System.Threading;
#endif


namespace LinesGame
{
    internal static class sc_Program
    {
        [method: STAThread]
        static void Main()
        {
            Application.SetHighDpiMode(HighDpiMode.SystemAware);
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);

            //log errors in release builds (debug builds intended to run under VisualStudio)
#if !DEBUG
            Application.ThreadException += onApplicationThreadException;   //message loop thread (UI)
            Application.SetUnhandledExceptionMode(UnhandledExceptionMode.CatchException);
            AppDomain.CurrentDomain.UnhandledException += onCurrentDomainThreadUnhandledException;
#endif

            c_UserStatisticsManager.st_pr_Instance.CountGameSession();

            ser_Coroutine.sc_CoroutineManager.st_StartUpdatingViaThreadPool();
            sc_Game.st_Start();

            Application.Run(new c_GameWnd());

            ser_Coroutine.sc_CoroutineManager.st_StopAllCoroutines();
            ser_Coroutine.sc_CoroutineManager.st_StopUpdating();

            if (c_ResourceManager.st_pr_bExists) {
                c_ResourceManager.st_pr_Instance.FreeAllResources();
            }

            c_UserStatisticsManager.st_pr_Instance.AddTime(( uint )sc_Game.st_pr_fGameTimeSeconds);
            c_UserStatisticsManager.st_pr_Instance.Save();
        }


#if !DEBUG
        private static void onApplicationThreadException(object sender, ThreadExceptionEventArgs e)
        {
            c_UserStatisticsManager.st_pr_Instance.CountException();
            sc_Logger.st_WriteLog(e.Exception, "Exception catched in UI thread.", null);
            MessageBox.Show("Error in game UI thread! Data was writen in log. Application will terminate. Sorry =(", c_GameWnd.sFORM_HEADER, MessageBoxButtons.OK, MessageBoxIcon.Error);
            Application.Exit(new System.ComponentModel.CancelEventArgs(cancel: true));
        }
        private static void onCurrentDomainThreadUnhandledException(object sender, UnhandledExceptionEventArgs e)
        {
            c_UserStatisticsManager.st_pr_Instance.CountException();
            sc_Logger.st_WriteLog((e.ExceptionObject as Exception)!, "Exception catched in Domain", null);
            MessageBox.Show("Error in Domain! Data was writen in log. Application is terminating. Sorry =(", c_GameWnd.sFORM_HEADER, MessageBoxButtons.OK, MessageBoxIcon.Error);
        }
#endif
    }
}
