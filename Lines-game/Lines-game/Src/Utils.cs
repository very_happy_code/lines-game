﻿#nullable enable

using global::System;
using System.IO;
using System.Runtime.CompilerServices;
using System.Reflection;
using System.Diagnostics;

using static LinesGame.sc_Debug;


namespace LinesGame
{
    internal static class sc_Debug
    {
        [method: System.Diagnostics.Conditional("DEBUG")]
        public static void st_Assert(bool condition, string? message, Exception? inner = null,
                [param: CallerMemberName]   string memberName__ = "",
                [param: CallerFilePath]     string filePath__   = "",
                [param: CallerLineNumber]   int lineNumber__    = 0
            )
        {
            if (!condition) {
                string fileName = System.IO.Path.GetFileName(filePath__);
                string exception_message = $"Assert at \"{fileName}\", in \"{memberName__}\", line {lineNumber__}. Message: \"" + (message ?? "---") + "\"";
                Debug.Print(exception_message);

                if (inner != null) {
                    sc_Logger.st_WriteLog(inner, exception_message, null);
                }
                else {
                    sc_Logger.st_WriteLog(exception_message);
                }

                throw new Exception(exception_message, inner);
            }
        }
    }


    internal static class sc_Logger
    {
        public static void st_WriteLog(string text)
        {
            var date = DateTime.Now;

            string log_file_name = "Log " + date.ToString("dd.MM.yyyy HH.mm.ss") + " .txt";

            sc_Utils.st_OpenStreamToWrite(sc_LinesDirectoriesHub.sLOGS_DIR, log_file_name, (stream) => {
                using StreamWriter sw = new(stream);

                sw.WriteLine($"Date: {date:dd.MM.ss}; Time: {date:HH:mm:ss}");
                sw.WriteLine(text);
            });
        }

        public static void st_WriteLog(Exception ex, string? headMessage, string? tailMessage)
        {
            string message_body = $"Error. Actual type is \"{ex.GetType().FullName}\" \n Message: {ex.Message ?? "---"} \n StackTrace: {ex.StackTrace}";
            st_WriteLog((headMessage ?? string.Empty) + Environment.NewLine + message_body + Environment.NewLine + (tailMessage ?? string.Empty));
        }

        public static void st_WriteLog(Exception ex)
        {
            st_WriteLog(ex, null, null);
        }
    }


    internal static class sc_ArrayExtensions
    {
        public static void ext_Clear<T>(this T[,] array)
        {
            Array.Clear(array, index: 0, length: array.Length);
        }
        public static void ext_Init<T>(this T[,] array)
            where T: new()
        {
            int dim0 = array.GetLength(dimension: 0);
            int dim1 = array.GetLength(dimension: 1);

            for (int i = 0; i < dim0; ++i) {
                for (int j = 0; j < dim1; ++j) {
                    array[i, j] = new T();
                }
            }
        }
        public static ref T ext_At<T>(this T[,] array, (int x, int y) pos)
        {
            return ref array[pos.x, pos.y];
        }
        public static void ext_DisposeAll<T>(this T[] array)
            where T : IDisposable
        {
            int length = array.Length;
            for (int i = 0; i < length; ++i) {
                array[i]?.Dispose();
            }
        }
        public static void ext_DisposeAll<T>(this T[,] array)
            where T : IDisposable
        {
            int wid = array.GetLength(dimension: 0);
            int hgt = array.GetLength(dimension: 1);

            for (int i = 0; i < wid; ++i) {
                for (int j = 0; j < hgt; ++j) {
                    array[i, j]?.Dispose();
                }
            }
        }
    }


    internal static class sc_Utils
    {
        public readonly struct ros_ReadonlyArrayWrapper<T>
            where T : struct
        {
            private readonly T[] ro_array;

            public readonly T this[int index]
            {
                get => GetCopy(index);
            }

            public ref readonly T GetRRef(int index) => ref ro_array[index];
            public T GetCopy(int index) => ro_array[index];

            public ros_ReadonlyArrayWrapper(T[] array) => this.ro_array = array;
        }


        public static T st_GetRandomEnumValue<T>(Random? random = null)
            where T : Enum
        {
            random ??= new Random();

            var values = Enum.GetValues(typeof(T));
            return ( T )values.GetValue(random.Next(values.Length))!;
        }

        public static int st_GetEnumValuesNum<T>()
            where T : struct, Enum
        {
            return Enum.GetValues<T>().Length;
        }

        public static string st_GetGameVersion()
        {
            return Assembly.GetExecutingAssembly().GetName().Version?.ToString() ?? string.Empty;
        }

        public static string? st_GetExecutingPath()
        {
            /*
             * Can't use Assembly.Location because of the build needs:
             * 'System.Reflection.Assembly.Location' always returns an empty string for assemblies embedded in a single-file app. If the path to the app directory is needed, consider calling 'System.AppContext.BaseDirectory'.
             */
            return Path.GetDirectoryName(AppContext.BaseDirectory /*Assembly.GetExecutingAssembly().Location*/);
        }

        public static Exception? st_OpenStreamToRead(string subdir, string file, Action<FileStream> whenOpen)
        {
            st_Assert(subdir != string.Empty, "Invalid param: " + nameof(subdir), new ArgumentException(nameof(subdir)));
            st_Assert(subdir != string.Empty, "Invalid param: " + nameof(file), new ArgumentException(nameof(file)));

            try {
                var dir = st_GetExecutingPath();
                if (dir == null)
                    throw new Exception("Can't get current executing directory!");

                dir = Path.Combine(dir, subdir);

                if (!Directory.Exists(dir))
                    throw new DirectoryNotFoundException($"Directory \"{dir}\" is not exists!");

                var full_path = Path.Combine(dir, file);

                if (!File.Exists(full_path))
                    throw new FileNotFoundException($"Can't find file: \"{full_path}\"");

                //I know what I am doing, you piece of sh**
#pragma warning disable IDE0063 // Use simple 'using' statement
                using (FileStream stream = new(full_path, FileMode.Open, FileAccess.Read)) {
                    whenOpen.Invoke(stream);
                    stream.Close();
                }
#pragma warning restore IDE0063 // Use simple 'using' statement
            }
            catch (Exception ex) {
                return ex;
            }
            return null;
        }
        public static Exception? st_OpenStreamToWrite(string subdir, string file, Action<FileStream> whenOpen)
        {
            st_Assert(subdir != string.Empty, "Invalid param: " + nameof(subdir), new ArgumentException(nameof(subdir)));
            st_Assert(subdir != string.Empty, "Invalid param: " + nameof(file), new ArgumentException(nameof(file)));

            try {
                var dir = st_GetExecutingPath();
                if (dir == null)
                    throw new Exception("Can't get current executing directory!");

                dir = Path.Combine(dir, subdir);

                if (!Directory.Exists(dir)) {
                    Directory.CreateDirectory(dir);
                }

                var path = Path.Combine(dir, file);

#pragma warning disable IDE0063 // Use simple 'using' statement
                using (FileStream stream = new(path, FileMode.Create, FileAccess.Write)) {
                    whenOpen.Invoke(stream);
                    stream.Close();
                }
#pragma warning restore IDE0063 // Use simple 'using' statement
            }
            catch (Exception ex) {
                return ex;
            }
            return null;
        }
    }


    internal class c_StateMachine<T> where T : struct, Enum
    {
#if DEBUG
        private const string sSTATE_MACHINE = "State machine: ";
#endif

        public T pr_State => _state;

        private T _state;

#if DEBUG
        public void SetState(T state, [param: CallerMemberName] string memberName = "")
        {
            Debug.Print(sSTATE_MACHINE + $"set state form \"{memberName}\". Prev state: \"{_state}\"; New state: \"{state}\"");
            _state = state;
        }
#else
        public void SetState(T state) => _state = state;
#endif

        public c_StateMachine(T state)
        {
            _state = state;
#if DEBUG
            Debug.Print(sSTATE_MACHINE + "initial state is " + state.ToString());
#endif
        }
    }


    /*
     * When attribute is applied to enum, all values of enum must be serial of indices (0, 1, 2, ... N).
     *  (All values from 0 to N must be present in the enum. There should be no gaps in values. Values may come in random order)
     */
    /// <summary>
    /// Attribute check that enum have numeric values that can be used as indices in arrays (all values are positive, contains 0, have no gaps)
    /// </summary>
    [type: AttributeUsage(validOn: AttributeTargets.Enum, Inherited = false, AllowMultiple = false)]
    internal class atr_IndexationEnumAttribute : Attribute
    {
        public static Exception? st_CheckConditionsForAllTypes()
        {
            var types = Assembly.GetCallingAssembly().GetTypes();

            foreach (var type in types) {
                if (type.GetCustomAttribute(typeof(atr_IndexationEnumAttribute)) is not null) {
                    var underlyingType = Enum.GetUnderlyingType(type);
                    var values = Enum.GetValues(type);
                    Array.Sort(values);

                    for (int i = 0; i < values.Length; ++i) {
                        object converted_i = Convert.ChangeType(( object )i, underlyingType);
                        if (!Enum.IsDefined(type, converted_i)) {
                            return new Exception($"The type \"{type.FullName}\" have inconsistency in indices! (index {i} is missed)");
                        }
                    }
                }
            }
            return null;
        }
    }


    /// <summary>
    /// Attribute check that type have no public ctors (use it on factory units).
    /// </summary>
    [type: AttributeUsage(validOn: AttributeTargets.Class, Inherited = true, AllowMultiple = false)]
    internal class atr_FactoryUnitAttribute : Attribute
    {
        public static Exception? st_CheckConditionsForAllTypes()
        {
            var types = Assembly.GetCallingAssembly().GetTypes();

            foreach(var type in types) {
                if (type.GetCustomAttribute(typeof(atr_FactoryUnitAttribute)) is not null) {
                    var public_ctors = type.GetConstructors(BindingFlags.Public | BindingFlags.Instance);
                    if (public_ctors.Length != 0) {
                        return new Exception($"The type \"{type.FullName}\" have public ctor, but it is factory unity!");
                    }
                }
            }
            return null;
        }
    }
}