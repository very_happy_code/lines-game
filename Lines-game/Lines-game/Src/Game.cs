﻿#nullable enable

using global::System;
using System.Threading;
using System.Linq;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

using static LinesGame.sc_Debug;

using ros_ColorsROWrapper_t = LinesGame.sc_Utils.ros_ReadonlyArrayWrapper<LinesGame.en_BallColors>;
using ros_LeaderboardEntry_t = LinesGame.c_Leaderboard.ros_LeaderboardEntry;


namespace LinesGame
{
    internal delegate void dl_GameOverCallback(uint score, float gameTimeSeconds);
    internal delegate void dl_TriggerSound(en_LinesSounds sound);


    [type: atr_IndexationEnum]
    internal enum en_LinesSounds : byte
    {
        eMove = 0x00,
        eNoMove,
        eSelect,
        eDestroy,
        eGameOver,
        eNotification   //for non-game events like turn cancellation
    }


    internal static class sc_Game
    {
        #region StructsAndEnums
        private class c_cell
        {
            public c_Ball? m_Ball;
            public int m_nPathData; //working data for path finding algorithm (stores wave front value)

            public bool HasBall() => m_Ball != null;
            public bool HasGrowedBall()
            {
                return m_Ball != null && m_Ball.pr_State == en_BallState.eGrowed;
            }
            public bool HasGrowedBallOfColor(en_BallColors color)
            {
                if (m_Ball != null) {
                    return (m_Ball.pr_State is en_BallState.eGrowed or en_BallState.eDestructing) && m_Ball.pr_Color == color;
                }
                return false;
            }

            public void SetupData(s_cellData data)
            {
                if (data.m_bHasBall) {
                    m_Ball = new c_Ball(data.m_BallColor, data.m_BallState);
                }
                else
                    m_Ball = null;
            }

            public c_cell()
            {
                this.m_Ball = null;
                this.m_nPathData = 0;
            }
        }

        [type: Serializable]
        private struct s_cellData
        {
            public bool m_bHasBall;
            public en_BallColors m_BallColor;
            public en_BallState m_BallState;

            public void SetupData(c_cell? cell)
            {
                if (cell?.m_Ball == null) {
                    m_bHasBall = false;
                    return;
                }
                else {
                    m_bHasBall = true;
                    m_BallColor = cell.m_Ball.pr_Color;
                    m_BallState = cell.m_Ball.pr_State;
                }
            }
        }

        [type: Serializable]
        private struct s_save
        {
            public string m_sGameVersion;
            public s_cellData[,] m_FieldData;
            public en_BallColors[] m_SpawnColors;
            public float m_fGameTimeSeconds;
            public uint m_unScore;
        }

        private enum en_gameStates : byte
        {
            eStart = 0x00,
            eStartPlayerTurn,
            eSelectingPosition,
            ePathFinding,
            eBallsMoving,
            eProcessingTurn,
            eBallsDestruction,
            eBallsStartGrowing,
            eBallsGrow,
            eFinalProcessing,
            eGameOver
        }

        private delegate bool dl_cellCheckingPredicate(c_cell cell);
        #endregion StructsAndEnums

        public const int nFIELD_WIDTH_CELLS = 9;  //measured in cells
        public const int nFIELD_HEIGHT_CELLS = nFIELD_WIDTH_CELLS;
        public const int nBALLS_TO_SPAWN = 3;

        private const string sNOT_STARTED_MSG = @"Game is not started";
        private const int nMIN_BALLS_IN_LINE_TO_DESTROY = 5;
        private const float fBALL_MOVING_SPEED = 20f;  //measured in cells per second
        private const double dANIMATIONS_AND_TIME_UPDATE_INTERVAL_MILLISECONDS = 33.0d;


        public static s_BallPosition? st_pr_SelectedBallPos { get => _selectedBallPos; }
        public static (int x, int y)? st_pr_DestinationPoint { get => _destinationPoint; }
        public static float st_pr_fGameTimeSeconds { get => _fGameTimeSeconds; }
        public static uint st_pr_unScore { get => _unScore; }

        public static event dl_GameOverCallback st_ev_GameOver
        {
            add {
                //don't allow to duplicate methods in invocation list:
                if (!(__dlGameOver?.GetInvocationList().Contains(value) ?? false)) {
                    __dlGameOver += value;
                }
            }
            remove => __dlGameOver -= value;
        }
        public static event dl_TriggerSound st_ev_TriggerSound
        {
            add {
                //don't allow to duplicate methods in invocation list:
                if (!(__dlTriggerSound?.GetInvocationList().Contains(value) ?? false)) {
                    __dlTriggerSound += value;
                }
            }
            remove => __dlTriggerSound -= value;
        }


        public static readonly c_Leaderboard st_m_ro_Leaderboard = new();
#if DEBUG
        public static volatile bool st_m_vl_bDEBUG_RaiceException = false;
#endif

        private static volatile bool vl_bIsStarted = false;
        private static Thread? logicThread = null;
        private static System.Timers.Timer animationsAndTimeUpdate = null!; //on elapsed, update all animations and count game time
        private static readonly object ro_oSyncRoot = new();
        private static readonly Random ro_random = new();
        private static readonly List<s_BallPosition> ro_path = new(capacity: nFIELD_WIDTH_CELLS * nFIELD_HEIGHT_CELLS / 2);
        private static readonly c_StateMachine<en_gameStates> ro_stateMachine;
        private static dl_GameOverCallback? __dlGameOver = null;
        private static dl_TriggerSound? __dlTriggerSound = null;
        //variables determinating game state i.e. state data (should be reseted at every new game):
        private static c_FreeBall? movingBall = null;   //ball that is moving through path
        private static s_BallPosition? _selectedBallPos = null;
        private static (int x, int y)? _destinationPoint = null;
        //variables that should be saved/loaded/reseted at every new game, also determinated game state:
        private static float _fGameTimeSeconds = .0f;
        private static uint _unScore;
        private static readonly c_cell[,] ro_field;
        private static readonly en_BallColors[] ro_nextSpawnColors = new en_BallColors[nBALLS_TO_SPAWN];
        //variables for turn cancelling (i.e. state backup):
        private static uint unPreviousScore;
        private static readonly en_BallColors[] ro_prevSpawnColors = new en_BallColors[nBALLS_TO_SPAWN];
        private static readonly s_cellData[,] ro_prevFieldState = new s_cellData[nFIELD_WIDTH_CELLS, nFIELD_HEIGHT_CELLS];
        private static bool bIsPrevStateValid = false;  //contains true if data above can be used to recover state


        public static void st_Start()
        {
            lock (ro_oSyncRoot) {
                st_Assert(!vl_bIsStarted, "Game already started", new InvalidOperationException());
                vl_bIsStarted = true;

                ro_stateMachine.SetState(en_gameStates.eStart);

                logicThread = new(gameLoop);
                logicThread.Name = "Game logic";
                logicThread.Start();

                animationsAndTimeUpdate = new System.Timers.Timer(interval: dANIMATIONS_AND_TIME_UPDATE_INTERVAL_MILLISECONDS);
                animationsAndTimeUpdate.Elapsed += ro_animationsAndTimeUpdate_Elapsed;
                animationsAndTimeUpdate.Start();
            }
        }

        public static void st_Stop()
        {
            lock (ro_oSyncRoot) {
                st_Assert(vl_bIsStarted, sNOT_STARTED_MSG, new InvalidOperationException());
                vl_bIsStarted = false;

                animationsAndTimeUpdate.Elapsed -= ro_animationsAndTimeUpdate_Elapsed;
                animationsAndTimeUpdate.Dispose();

                while (logicThread!.IsAlive) {
                    Thread.Sleep(millisecondsTimeout: 10);
                }
            }
        }
        public static void st_Restart()
        {
            lock (ro_oSyncRoot) {
                st_Assert(vl_bIsStarted, sNOT_STARTED_MSG, new InvalidOperationException());

                clearSave();

                ro_stateMachine.SetState(en_gameStates.eStart);
            }
        }
#if DEBUG
        public static void st_DEBUG_RestartWithSaveLoading()
        {
            lock (ro_oSyncRoot) {
                st_Assert(vl_bIsStarted, sNOT_STARTED_MSG, new InvalidOperationException());

                ro_stateMachine.SetState(en_gameStates.eStart);
            }
        }
#endif
        public static bool st_CanSave() => ro_stateMachine.pr_State == en_gameStates.eSelectingPosition;
        public static void st_Save(out string? errorMessage)
        {
            lock (ro_oSyncRoot) {
                st_Assert(vl_bIsStarted, sNOT_STARTED_MSG, new InvalidOperationException());

                s_save save = new();

                /* create save data: */
                {
                    save.m_sGameVersion = sc_Utils.st_GetGameVersion();
                    save.m_unScore = _unScore;
                    save.m_fGameTimeSeconds = _fGameTimeSeconds;
                    save.m_SpawnColors = ro_nextSpawnColors;

                    save.m_FieldData = new s_cellData[nFIELD_WIDTH_CELLS, nFIELD_HEIGHT_CELLS];
                    for (int x = 0; x < nFIELD_WIDTH_CELLS; ++x) {
                        for (int y = 0; y < nFIELD_HEIGHT_CELLS; ++y) {
                            save.m_FieldData[x, y].SetupData(ro_field[x, y]);
                        }
                    }
                }

                var error = sc_Utils.st_OpenStreamToWrite(sc_LinesDirectoriesHub.sSAVE_DIR, sc_LinesDirectoriesHub.sSAVE_FILE, (FileStream stream) => {
                    BinaryFormatter formatter = new();
#pragma warning disable SYSLIB0011 // Type or member is obsolete
                    formatter.Serialize(stream, save);
#pragma warning restore SYSLIB0011 // Type or member is obsolete
                });

                st_m_ro_Leaderboard.SaveOnDisk(out string? leaderboard_message);

                errorMessage = error?.Message ?? leaderboard_message;
            }
        }
        public static void st_CancelLastTurn()
        {
            st_Assert(ro_stateMachine.pr_State == en_gameStates.eSelectingPosition, "Invalid state! State is: " + ro_stateMachine.pr_State.ToString(), new InvalidOperationException());

            if (!st_CanCancelLastTurn()) {
                return;
            }

            lock (ro_oSyncRoot) {
                if (!bIsPrevStateValid) {
                    return;
                }

                _unScore = unPreviousScore;

                for (int i = 0; i < nBALLS_TO_SPAWN; ++i) {
                    ro_nextSpawnColors[i] = ro_prevSpawnColors[i];
                }

                for (int x = 0; x < nFIELD_WIDTH_CELLS; ++x) {
                    for (int y = 0; y < nFIELD_HEIGHT_CELLS; ++y) {
                        ro_field[x, y].SetupData(ro_prevFieldState[x, y]);
                    }
                }

                // (!!!) note, that game time should not be recovered when cancelling turn

                ro_stateMachine.SetState(en_gameStates.eStartPlayerTurn);

                bIsPrevStateValid = false;

                __dlTriggerSound?.Invoke(en_LinesSounds.eNotification);
            }
        }
        public static bool st_CanCancelLastTurn() => bIsPrevStateValid && ro_stateMachine.pr_State == en_gameStates.eSelectingPosition;
        public static void st_UserSelectCell(int x, int y)
        {
#if DEBUG
            st_Assert(vl_bIsStarted, sNOT_STARTED_MSG, new InvalidOperationException());

            if (x is < 0 or >= nFIELD_WIDTH_CELLS) {
                st_Assert(false, $"{nameof(x)} = {x}; width: {nFIELD_WIDTH_CELLS}", new ArgumentOutOfRangeException(nameof(x)));
            }
            else if (y is < 0 or >= nFIELD_HEIGHT_CELLS) {
                st_Assert(false, $"{nameof(y)} = {y}; height: {nFIELD_HEIGHT_CELLS}", new ArgumentOutOfRangeException(nameof(y)));
            }
#else
            //ignore incorrect input (actually this should not happend):
            if (x < 0 || y < 0 || x >= nFIELD_WIDTH_CELLS || y >= nFIELD_HEIGHT_CELLS) {
                return;
            }
#endif

            if (ro_stateMachine.pr_State == en_gameStates.eSelectingPosition) {
                if (ro_field[x, y].HasGrowedBall()) {
                    if (_selectedBallPos != null) {
                        var (old_x, old_y) = _selectedBallPos.Value;
                        //stop animating previous selection:
                        if (ro_field[old_x, old_y].HasGrowedBall()) {
                            ro_field[old_x, old_y].m_Ball!.m_CurrentAnim = null;
                        }
                    }

                    _selectedBallPos = new() { m_nX = x, m_nY = y };

                    //animate current selection:
                    ro_field[x, y].m_Ball!.m_CurrentAnim = sc_LinesResourcesProvider.st_GetNewBallAnimation(en_BallAnimations.eBallJumping, ro_field[x, y].m_Ball!.pr_Color);

                    //sound:
                    __dlTriggerSound?.Invoke(en_LinesSounds.eSelect);
                }
                else if (_selectedBallPos != null) {  //if selected cell is empty or ball is not grown && we have some selected ball (_selectedBallPos is not null)
                    _destinationPoint = (x, y);
                }
            }
        }
#if DEBUG
        public static void st_DEBUG_SpawnBallAt(int x, int y, en_BallColors color)
        {
            if (x is >= 0 and < nFIELD_WIDTH_CELLS) {
                if (y is >= 0 and < nFIELD_HEIGHT_CELLS)
                    ro_field[x, y].m_Ball = new c_Ball(color, en_BallState.eGrowed);
            }
        }
        public static void st_DEBUG_ClearCell(int x, int y)
        {
            if (x is >= 0 and < nFIELD_WIDTH_CELLS)
                if (y is >= 0 and < nFIELD_HEIGHT_CELLS) {
                    ro_field[x, y].m_Ball = null;
                }
        }
        public static void st_DEBUG_FillWithRandomBalls()
        {
            foreach(var cell in ro_field) {
                if (cell.m_Ball == null) 
                    cell.m_Ball = new c_Ball(sc_Utils.st_GetRandomEnumValue<en_BallColors>(ro_random), en_BallState.eGrowed);
            }
        }
#endif
        public static r_BallDTO? st_GetBallAt(int x, int y)
        {
#if DEBUG
            st_Assert(vl_bIsStarted, sNOT_STARTED_MSG, new InvalidOperationException());

            if (x is < 0 or >= nFIELD_WIDTH_CELLS) {
                st_Assert(false, $"{nameof(x)} = {x}; width: {nFIELD_WIDTH_CELLS}", new ArgumentOutOfRangeException(nameof(x)));
            }
            else if (y is < 0 or >= nFIELD_HEIGHT_CELLS) {
                st_Assert(false, $"{nameof(y)} = {y}; height: {nFIELD_HEIGHT_CELLS}", new ArgumentOutOfRangeException(nameof(y)));
            }
#endif

            if (ro_field[x, y].HasBall()) {
                var ball = ro_field[x, y].m_Ball!;
                return new r_BallDTO(ball.pr_Color, ball.pr_State, ball.m_CurrentAnim);
            }
            else
                return null;
        }
        public static r_MovingBallDTO? st_GetMovingBall()
        {
            if (movingBall == null)
                return null;
            else
                return new r_MovingBallDTO(movingBall.m_Position, movingBall.pr_Color, movingBall.pr_State, movingBall.m_CurrentAnim);
        }
        public static ros_ColorsROWrapper_t st_GetNextSpawnColors() => new(ro_nextSpawnColors);
        public static IReadOnlyList<s_BallPosition> st_GetBallMovingPath()
        {
            return ro_path;
        }
#if DEBUG
        public static void st_DEBUG_SetGameTime(float seconds)
        {
            _fGameTimeSeconds = seconds;
        }
#endif


        private static void gameLoop()
        {
            int removed_balls_on_curr_turn = 0;
            c_Ball? overlapped_ball = null; //if we place moving ball on top of small ball, save overlapped ball here
            ser_Coroutine? ball_moving_coroutine = null;

            while (vl_bIsStarted) {
#if DEBUG
                if (st_m_vl_bDEBUG_RaiceException) {
                    throw new Exception("This is a test exception (in game thread)!");
                }
#endif

                switch (ro_stateMachine.pr_State) {
                    case en_gameStates.eStart: {
                            //reset game data:
                            _selectedBallPos = null;
                            _destinationPoint = null;
                            movingBall = null;
                            removed_balls_on_curr_turn = 0;
                            overlapped_ball = null;
                            bIsPrevStateValid = false;

                            if (isSaveFileExists()) {
                                if (!loadSaveFile()) {
                                    clearSave();
                                    continue;   //execute the same state again
                                }
                            }
                            else {
                                ro_field.ext_Init();    //fill with empty cells
                                generateRandomSpawnColorsForNextSpawn();
                                spawnBalls(en_BallState.eGrowed);
                                spawnBalls(en_BallState.eNotGrowed);
                                _unScore = 0u;
                                _fGameTimeSeconds = .0f;
                            }
                            ro_stateMachine.SetState(en_gameStates.eStartPlayerTurn);
                        }
                        break;

                    case en_gameStates.eStartPlayerTurn: {
                            _selectedBallPos = null;
                            _destinationPoint = null;
                            ro_stateMachine.SetState(en_gameStates.eSelectingPosition);
                        }
                        continue;

                    case en_gameStates.eSelectingPosition: {
                            if (_destinationPoint != null) {
                                ro_stateMachine.SetState(en_gameStates.ePathFinding);
                                collectDataForTurnCancellation();
                                continue;
                            }
                        }
                        break;

                    case en_gameStates.ePathFinding: {
                            fillPathFindingData(out bool path_exists);

                            if (path_exists) {
                                st_Assert(movingBall == null, nameof(movingBall) + " must be null before and after moving state!");
                                ro_stateMachine.SetState(en_gameStates.eBallsMoving);
                            }
                            else {  //path is not exists:
                                //play sound:
                                __dlTriggerSound?.Invoke(en_LinesSounds.eNoMove);
                                //continue player turn:
                                _destinationPoint = null;
                                ro_stateMachine.SetState(en_gameStates.eSelectingPosition);
                            }
                        }
                        continue;

                    case en_gameStates.eBallsMoving: {
                            st_Assert(_selectedBallPos != null, nameof(_selectedBallPos) + " is null", new NullReferenceException());
                            st_Assert(_destinationPoint != null, nameof(_destinationPoint) + " is null", new NullReferenceException());

                            //if moving is not started yet:
                            if (movingBall == null) {
                                var (x, y) = _selectedBallPos!.Value;
                                movingBall = new c_FreeBall(_selectedBallPos!.Value, ro_field[x, y].m_Ball!);

                                ro_field[x, y].m_Ball = null;   //remove stationary ball

                                //if we place a ball on top of another growing ball- save overlapped ball in temporal variable:
                                if (ro_field.ext_At(_destinationPoint!.Value).HasBall()) {
                                    overlapped_ball = ro_field.ext_At(_destinationPoint!.Value).m_Ball;

                                    st_Assert(overlapped_ball!.pr_State == en_BallState.eNotGrowed, "Incorrect overlapped ball state: " + overlapped_ball!.pr_State); //path finding state guarantee this
                                }

                                //start moving via coroutine:
                                ball_moving_coroutine = ser_Coroutine.st_Start(ballMovingCoroutine);

                                //play sound:
                                __dlTriggerSound?.Invoke(en_LinesSounds.eMove);
                            }
                            //if moving is in progress but not finished:
                            else if (ball_moving_coroutine!.pr_bIsExecuting) {
                                break;  //wait for next frame and repeat checking
                            }
                            //if moving is finished:
                            else {
                                ball_moving_coroutine = null;

                                //place new stationary ball in destination point:
                                ro_field.ext_At(_destinationPoint!.Value).m_Ball = new c_Ball(movingBall.pr_Color, movingBall.pr_State);
                                movingBall = null;

                                ro_stateMachine.SetState(en_gameStates.eProcessingTurn);
                            }
                        }
                        break;

                    case en_gameStates.eProcessingTurn: {
                            st_Assert(movingBall == null, nameof(movingBall) + " must be null before and after moving state!");

                            findLinesAndMarkBallsForDestruction(out int markedBalls);

                            //if we have some balls to destory but did not destroy any balls on current turn yet:
                            if (markedBalls != 0 && removed_balls_on_curr_turn == 0) {
                                //play destruction sound (this sound should be played no more than once at a turn):
                                __dlTriggerSound?.Invoke(en_LinesSounds.eDestroy);
                            }

                            ro_stateMachine.SetState(en_gameStates.eBallsDestruction);
                        }
                        continue;

                    case en_gameStates.eBallsDestruction: {
                            if (!checkAnimationFinishedForBallsWithState(en_BallState.eDestructing)) {
                                break;  //continue with this state
                            }

                            removeMarkedBalls(ref removed_balls_on_curr_turn);

                            if (removed_balls_on_curr_turn == 0)
                                ro_stateMachine.SetState(en_gameStates.eBallsStartGrowing);
                            else {
                                if (overlapped_ball != null) {
                                    //when we overlap growing ball and destroy a line at same turn then overlapped ball should be free now
                                    //NOTE: in debug mode we can manually create situation when this assertion fails, but when we play without cheating this situation must not happen
                                    st_Assert(!ro_field.ext_At(_destinationPoint!.Value).HasBall(), "Field inconsistency at position: " + _destinationPoint!.Value.ToString());

                                    ro_field.ext_At(_destinationPoint!.Value).m_Ball = overlapped_ball;
                                    overlapped_ball = null;
                                }
                                ro_stateMachine.SetState(en_gameStates.eFinalProcessing);   //skip growing if destroy some lines
                            }
                        }
                        continue;

                    case en_gameStates.eBallsStartGrowing: {
                            startGrowingBalls();
                            overlapped_ball?.StartGrowing();
                            ro_stateMachine.SetState(en_gameStates.eBallsGrow);
                        }
                        continue;

                    case en_gameStates.eBallsGrow: {
                            //if we had an overlap - relocate a ball
                            if (overlapped_ball != null) {
                                placeBallInRandomPos(overlapped_ball, countFreeSpace());
                                overlapped_ball = null;
                            }

                            if (!checkAnimationFinishedForBallsWithState(en_BallState.eGrowing)) {
                                break;  //continue with this state
                            }

                            //NOTE: we have no special sound for balls growing
                            //__dlTriggerSound?.Invoke(en_LinesSounds.eMove);

                            growBalls(out int growed_balls_count);

                            //if some balls were growed, check if new lines appear:
                            if (growed_balls_count != 0)
                                ro_stateMachine.SetState(en_gameStates.eProcessingTurn);
                            else
                                ro_stateMachine.SetState(en_gameStates.eFinalProcessing); //if all existing small balls were growed - finish turn
                        }
                        continue;

                    case en_gameStates.eFinalProcessing: {
                            /* scoring: */
                            if (removed_balls_on_curr_turn > 0) {
                                st_Assert(removed_balls_on_curr_turn >= 5, "Incorrect removed balls count: " + removed_balls_on_curr_turn.ToString());
                                //add 8 score points for 5 first balls:
                                removed_balls_on_curr_turn -= 5;
                                _unScore += 8u;

                                //than +2 points for each extra ball:
                                _unScore += ( uint )(removed_balls_on_curr_turn * 2);

                                removed_balls_on_curr_turn = 0;
                            }

                            //add new small balls if we have no one left:
                            if (countNotGrowedBalls() == 0) {
                                spawnBalls(en_BallState.eNotGrowed);
                            }

                            //if there is no more free space - Game Over
                            if (countCellsWithoutGrowedBalls() == 0)
                                ro_stateMachine.SetState(en_gameStates.eGameOver);
                            else
                                ro_stateMachine.SetState(en_gameStates.eStartPlayerTurn);
                        }
                        continue;

                    case en_gameStates.eGameOver: {
                            //save in scoreboard:
                            ros_LeaderboardEntry_t entry = new(Environment.UserName, _unScore, _fGameTimeSeconds, DateTime.Now.ToString(@"dd.MM.yyyy"));
                            sc_Game.st_m_ro_Leaderboard.AddEntry(in entry);

                            __dlTriggerSound?.Invoke(en_LinesSounds.eGameOver);

                            __dlGameOver?.Invoke(_unScore, _fGameTimeSeconds);
                        }
                        break;

#if DEBUG
                    default:
                        st_Assert(false, "Unexpected state: " + ro_stateMachine.pr_State.ToString());
                        break;
#endif
                }

                Thread.Sleep(millisecondsTimeout: 1);
            }
        }

        private static void ro_animationsAndTimeUpdate_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            const float DT_SECONDS = ( float )(dANIMATIONS_AND_TIME_UPDATE_INTERVAL_MILLISECONDS / 1000.0d);

            if (ro_stateMachine.pr_State != en_gameStates.eGameOver) {
                _fGameTimeSeconds += DT_SECONDS;
            }

            animateBalls(DT_SECONDS);
        }

        private static void animateBalls(float dtSeconds)
        {
            foreach(var cell in ro_field) {
                cell.m_Ball?.UpdateAnim(dtSeconds);
                movingBall?.UpdateAnim(dtSeconds);
            }
        }

        private static IEnumerable<I_CoroutineBehavior> ballMovingCoroutine()
        {
            st_Assert(movingBall != null, nameof(movingBall) + " is null", new NullReferenceException());
            st_Assert(ro_stateMachine.pr_State == en_gameStates.eBallsMoving, "Invalid state! Current state = " + ro_stateMachine.pr_State.ToString(), new InvalidOperationException());

            while (!movingBall!.m_Position.Equals(_destinationPoint!.Value)) {
                movingBall!.m_Position = ro_path[ro_path.IndexOf(movingBall.m_Position) + 1];
                yield return new s_CoroutineWaitForSeconds(1.0f / fBALL_MOVING_SPEED);
            }
        }

        private static string getSaveFilePath() => Path.Combine(sc_Utils.st_GetExecutingPath()!, sc_LinesDirectoriesHub.sSAVE_DIR, sc_LinesDirectoriesHub.sSAVE_FILE);

        private static void clearSave()
        {
            if (isSaveFileExists()) {
                File.Delete(getSaveFilePath()!);
            }
        }

        private static bool isSaveFileExists()
        {
            string? path = getSaveFilePath();

            if (path is null) {
                return false;
            }

            return File.Exists(path);
        }

        private static bool loadSaveFile()
        {
            st_Assert(isSaveFileExists(), "Save file is not exists!", new InvalidOperationException());

            bIsPrevStateValid = false;


            var error = sc_Utils.st_OpenStreamToRead(sc_LinesDirectoriesHub.sSAVE_DIR, sc_LinesDirectoriesHub.sSAVE_FILE, static (FileStream stream) => {
                s_save save;
                BinaryFormatter formatter = new();

#pragma warning disable SYSLIB0011
                save = ( s_save )formatter.Deserialize(stream);
#pragma warning restore SYSLIB0011

                if (!save.m_sGameVersion?.Equals(sc_Utils.st_GetGameVersion()) ?? true) {
                    throw new Exception($"Save file version inconsistency! Save file has version \"{save.m_sGameVersion}\", game have version: {sc_Utils.st_GetGameVersion()}");
                }

                _unScore = save.m_unScore;
                _fGameTimeSeconds = save.m_fGameTimeSeconds;
                Array.Copy(save.m_SpawnColors, ro_nextSpawnColors, nBALLS_TO_SPAWN);

                for (int x = 0; x < nFIELD_WIDTH_CELLS; ++x) {
                    for (int y = 0; y < nFIELD_HEIGHT_CELLS; ++y) {
                        ro_field[x, y].SetupData(save.m_FieldData[x, y]);
                    }
                }
            });

#if DEBUG
            if (error != null) {
                System.Diagnostics.Debug.Print("Loading error: " + error.Message);
            }
#endif
            if (error != null) {
                sc_Logger.st_WriteLog(error, "Can't load save: ", null);
            }

            return error == null;
        }

        private static void collectDataForTurnCancellation()
        {
            bIsPrevStateValid = false;

            unPreviousScore = _unScore;

            for (int i = 0; i < nBALLS_TO_SPAWN; ++i) {
                ro_prevSpawnColors[i] = ro_nextSpawnColors[i];
            }

            for (int x = 0; x < nFIELD_WIDTH_CELLS; ++x) {
                for (int y = 0; y < nFIELD_HEIGHT_CELLS; ++y) {
                    ro_prevFieldState[x, y].SetupData(ro_field[x, y]);
                }
            }


            bIsPrevStateValid = true;
        }

        private static void generateRandomSpawnColorsForNextSpawn()
        {
            for (int i = 0; i < nBALLS_TO_SPAWN; ++i) {
                ro_nextSpawnColors[i] = sc_Utils.st_GetRandomEnumValue<en_BallColors>(ro_random);
            }
        }

        private static void spawnBalls(en_BallState initialState)
        {
            int remaining_balls_to_spawn = nBALLS_TO_SPAWN;
            int free_cells_num = countFreeSpace();

            if (free_cells_num < remaining_balls_to_spawn) remaining_balls_to_spawn = free_cells_num;

            while (remaining_balls_to_spawn > 0) {
                placeBallInRandomPos(freeCellsNumber: free_cells_num--, ball: new c_Ball(ro_nextSpawnColors[--remaining_balls_to_spawn], initialState));
            }

            generateRandomSpawnColorsForNextSpawn();
        }
        private static void placeBallInRandomPos(c_Ball ball, int freeCellsNumber)
        {
            st_Assert(freeCellsNumber > 0, "Can't place the ball when there is no free cells", new InvalidOperationException());

            int selected_free_cell_ind = ro_random.Next(minValue: 0, maxValue: freeCellsNumber);
            int current_free_cell_ind = 0;

            for (int x = 0; x < nFIELD_WIDTH_CELLS; ++x) {
                for (int y = 0; y < nFIELD_HEIGHT_CELLS; ++y) {

                    if (!ro_field[x, y].HasBall()) {
                        if (current_free_cell_ind == selected_free_cell_ind) {
                            ro_field[x, y].m_Ball = ball;
                            return;
                        }
                        else
                            current_free_cell_ind++;
                    }

                }
            }

            st_Assert(false, $"There is no actual free cells; Func arg {nameof(freeCellsNumber)} = {freeCellsNumber}");
        }

        private static int countFreeSpace()
        {
            int result = 0;

            foreach (var cell in ro_field) {
                if (!cell.HasBall()) result++;
            }

            return result;
        }

        private static int countCellsWithoutGrowedBalls()
        {
            int result = 0;

            foreach (var cell in ro_field) {
                if (!cell.HasGrowedBall()) result++;
            }

            return result;
        }

        private static int countNotGrowedBalls()
        {
            int result = 0;

            foreach(var cell in ro_field) {
                if (cell.HasBall() && cell.m_Ball!.pr_State == en_BallState.eNotGrowed) {
                    result++;
                }
            }
            return result;
        }

        private static void fillPathFindingData(out bool pathExists)
        {
            st_Assert(_destinationPoint != null, nameof(_destinationPoint) + " is null", new NullReferenceException());
            st_Assert(_selectedBallPos != null, nameof(_selectedBallPos) + " is null", new NullReferenceException());

            const int nEMPTY_PATH_DATA = -1;

            static bool spread_a_wave(int x, int y, int waveFront)
            {
                if (x is >= 0 and < nFIELD_WIDTH_CELLS) {
                    if (y is >= 0 and < nFIELD_HEIGHT_CELLS) {

                        //if cell has no path data from current wave:
                        if (ro_field[x, y].m_nPathData == nEMPTY_PATH_DATA) {

                            var (target_x, target_y) = _selectedBallPos!.Value;
                            //if cell is free or contains path destination point:
                            if (!ro_field[x, y].HasGrowedBall() || (x, y) == (target_x, target_y)) {
                                ro_field[x, y].m_nPathData = waveFront;
                                return true;
                            }

                        }

                    }
                }
                return false;
            }

            //clear path finding data:
            foreach (var cell in ro_field) {
                cell.m_nPathData = nEMPTY_PATH_DATA;
            }

            //spread a wave:
            int wave_front = 0;
            //wave start spreading from destination point:
            ro_field.ext_At(_destinationPoint!.Value).m_nPathData = wave_front;
            bool spreading;

            do {
                spreading = false;

                for (int x = 0; x < nFIELD_WIDTH_CELLS; ++x) {
                    for (int y = 0; y < nFIELD_HEIGHT_CELLS; ++y) {
                        //find wave front and spread a wave on neighbor cells:
                        if (ro_field[x, y].m_nPathData == wave_front) {
                            //if we reach start point - path is finded:
                            if (x == _selectedBallPos!.Value.m_nX && y == _selectedBallPos!.Value.m_nY) {
                                goto lbl_path_exists;   //I just wanna use this evil command =)
                            }

                            spreading |= spread_a_wave(x - 1, y, wave_front + 1);
                            spreading |= spread_a_wave(x + 1, y, wave_front + 1);
                            spreading |= spread_a_wave(x, y + 1, wave_front + 1);
                            spreading |= spread_a_wave(x, y - 1, wave_front + 1);
                        }
                    }
                }

                wave_front++;
            } while (spreading);

            pathExists = false;
            return;

lbl_path_exists:
            pathExists = true;
            constructPath();
        }
        private static void constructPath()
        {
            st_Assert(_destinationPoint != null, nameof(_destinationPoint) + " is null", new NullReferenceException());
            st_Assert(_selectedBallPos != null, nameof(_selectedBallPos) + " is null", new NullReferenceException());

            ro_path.Clear();

            var (x, y) = _selectedBallPos!.Value;
            ro_path.Add(new s_BallPosition() { m_nX = x, m_nY = y });

            int wave_front = ro_field[x, y].m_nPathData;

            void move_to_next_point()
            {
                if (x > 0 && ro_field[x - 1, y].m_nPathData >= 0 && ro_field[x - 1, y].m_nPathData < wave_front) {
                    x--;
                }
                else if (x < nFIELD_WIDTH_CELLS - 1 && ro_field[x + 1, y].m_nPathData >= 0 &&  ro_field[x + 1, y].m_nPathData < wave_front) {
                    x++;
                }
                else if (y > 0 && ro_field[x, y - 1].m_nPathData >= 0 &&  ro_field[x, y - 1].m_nPathData < wave_front) {
                    y--;
                }
                else if (y < nFIELD_HEIGHT_CELLS - 1 && ro_field[x, y + 1].m_nPathData >= 0 &&  ro_field[x, y + 1].m_nPathData < wave_front) {
                    y++;
                }
                wave_front--;
            }

            while (wave_front > 0) {
                move_to_next_point();
                ro_path.Add(new s_BallPosition() { m_nX = x, m_nY = y });
            }

            st_Assert(_destinationPoint == (x, y), "Path data inconsistency!");
        }

        private static void startGrowingBalls()
        {
            foreach (var cell in ro_field) {
                if (cell.HasBall() && cell.m_Ball!.pr_State == en_BallState.eNotGrowed) {
                    cell.m_Ball!.StartGrowing();
                }
            }
        }
        private static void growBalls(out int growedBallsCount) //finish growing
        {
            growedBallsCount = 0;

            foreach(var cell in ro_field) {
                if (cell.HasBall() && cell.m_Ball!.pr_State == en_BallState.eGrowing) {
                    cell.m_Ball!.Grow();
                    growedBallsCount++;
                }
            }
        }

        private static void findLinesAndMarkBallsForDestruction(out int markedBallsCount)
        {
            static void bidirectional_pass(int x, int y, int dx, int dy, dl_cellCheckingPredicate predicate)
            {
                for (int i = x, j = y; i >= 0 && j >= 0 && i < nFIELD_WIDTH_CELLS && j < nFIELD_HEIGHT_CELLS; i += dx, j += dy) {
                    if (!predicate.Invoke(ro_field[i, j])) {
                        break;
                    }
                }
                x -= dx;
                y -= dy;
                for (int i = x, j = y; i >= 0 && j >= 0 && i < nFIELD_WIDTH_CELLS && j < nFIELD_HEIGHT_CELLS; i -= dx, j -= dy) {
                    if (!predicate.Invoke(ro_field[i, j])) {
                        break;
                    }
                }
            }

            static void bidirectional_removal(int x, int y, int dx, int dy, List<c_cell> removal_list, ref int count)
            {
                var color = ro_field[x, y].m_Ball!.pr_Color;
                removal_list.Clear();

                bidirectional_pass(x, y, dx, dy, delegate (c_cell cell) {
                    if (cell.HasGrowedBallOfColor(color)) {
                        removal_list.Add(cell);
                        return true;
                    }
                    return false;
                });

                if (removal_list.Count >= nMIN_BALLS_IN_LINE_TO_DESTROY) {
                    foreach (var cell in removal_list) {
                        cell.m_Ball!.Destroy();
                        count++;
                    }
                }
            }

            static void check_directions(int x, int y, List<c_cell> removal_list, ref int count)
            {
                //horizontal:
                bidirectional_removal(x, y, dx: +1, dy: 0, removal_list, ref count);

                //vertical:
                bidirectional_removal(x, y, dx: 0, dy: +1, removal_list, ref count);

                //diag:
                bidirectional_removal(x, y, dx: +1, dy: +1, removal_list, ref count);

                bidirectional_removal(x, y, dx: -1, dy: +1, removal_list, ref count);
            }

            markedBallsCount = 0;
            List<c_cell> removal_list = new(capacity: nFIELD_WIDTH_CELLS);

            for (int x = 0; x < nFIELD_WIDTH_CELLS; ++x) {
                for (int y = 0; y < nFIELD_HEIGHT_CELLS; ++y) {
                    if (ro_field[x, y].HasGrowedBall()) {
                        check_directions(x, y, removal_list, ref markedBallsCount);
                    }
                }
            }
        }

        private static void removeMarkedBalls(ref int removedBallsCounter)
        {
            foreach (var cell in ro_field) {
                if (cell.m_Ball?.pr_State == en_BallState.eDestructing) {
                    cell.m_Ball = null;
                    removedBallsCounter++;
                }
            }
        }

        private static bool checkAnimationFinishedForBallsWithState(en_BallState state)
        {
            foreach(var cell in ro_field) {
                if (cell.m_Ball?.pr_State == state) {
                    //don't consider looped animations:
                    if (cell.m_Ball.m_CurrentAnim?.m_bWrapTime ?? false) {
                        continue;
                    }
                    //animation is not finished on some ball:
                    if (!cell.m_Ball!.m_CurrentAnim?.pr_bIsFinished ?? false) {
                        return false;
                    }
                }
            }
            return true;
        }


        static sc_Game()
        {
            ro_field = new c_cell[nFIELD_WIDTH_CELLS, nFIELD_HEIGHT_CELLS];
            ro_field.ext_Init();    //fill with empty cells
            ro_stateMachine = new c_StateMachine<en_gameStates>(en_gameStates.eStart);
        }
    }
}