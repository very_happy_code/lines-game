﻿#nullable enable

using global::System;
using System.IO;
using System.Xml.Serialization;


namespace LinesGame
{
    [type: Serializable]
    public record r_StatisticsFile(uint pr_unPlayTimeSeconds, uint pr_unExceptions, uint pr_unSessionsCount)
    {
        public string pr_sHumanReadableTime
        {
            get {
                TimeSpan ts = new(0, 0, ( int )pr_unPlayTimeSeconds);
                return ts.ToString();
            }
            set { /* noop */ }  //do not remove (for serialization needs)
        }


        public r_StatisticsFile()
            : this(0, 0, 0)
        { /* noop */ }
    }


    internal class c_UserStatisticsManager
    {
        private static c_UserStatisticsManager? _st_instance = null;

        public static c_UserStatisticsManager st_pr_Instance
        {
            get {
                _st_instance ??= new c_UserStatisticsManager();
                return _st_instance;
            }
        }
        //--------------------------------------------------------------------------------------------------------------

        private uint _unTotalPlayTimeAcrossAllSessionsSeconds;
        private uint _unExceptionsCountAcrossAllSessions;
        private uint _unGameSessionsCount;


        public uint pr_unTotalPlayTimeAcrossAllSessionsSeconds { get => _unTotalPlayTimeAcrossAllSessionsSeconds; }
        public uint pr_unExceptionsCountAcrossAllSessions { get => _unExceptionsCountAcrossAllSessions; }
        public uint pr_unGameSessionsCount { get => _unGameSessionsCount; }


        public void AddTime(uint dtSeconds)
        {
            unchecked { _unTotalPlayTimeAcrossAllSessionsSeconds += dtSeconds; }
        }
        public void CountException()
        {
            unchecked { _unExceptionsCountAcrossAllSessions++; }
        }
        public void CountGameSession()
        {
            unchecked { _unGameSessionsCount++; }
        }
        public void Save()
        {
            var error = sc_Utils.st_OpenStreamToWrite(sc_LinesDirectoriesHub.sUSER_STATISTICS_DIR, sc_LinesDirectoriesHub.sUSER_STATISTICS_FILE, (FileStream stream) => {
                XmlSerializer serializer = new(typeof(r_StatisticsFile));

                r_StatisticsFile file = new(_unTotalPlayTimeAcrossAllSessionsSeconds, _unExceptionsCountAcrossAllSessions, _unGameSessionsCount);
                serializer.Serialize(stream, file);
            });

            if (error != null)
                sc_Logger.st_WriteLog(error, "Error while writing user statistics: ", "");
        }


        private c_UserStatisticsManager()
        {
            this._unTotalPlayTimeAcrossAllSessionsSeconds = 0u;
            this._unExceptionsCountAcrossAllSessions = 0u;
            this._unGameSessionsCount = 0u;

            var error = sc_Utils.st_OpenStreamToRead(sc_LinesDirectoriesHub.sUSER_STATISTICS_DIR, sc_LinesDirectoriesHub.sUSER_STATISTICS_FILE, delegate (FileStream stream) {
                XmlSerializer serializer = new(typeof(r_StatisticsFile));

                if (serializer.Deserialize(stream) is r_StatisticsFile file) {
                    _unTotalPlayTimeAcrossAllSessionsSeconds = file.pr_unPlayTimeSeconds;
                    _unExceptionsCountAcrossAllSessions = file.pr_unExceptions;
                    _unGameSessionsCount = file.pr_unSessionsCount;
                }
            });

            if (error != null)
                sc_Logger.st_WriteLog(error, "Error while reading user statistics: ", null);
        }
    }
}