﻿#nullable enable

using global::System;
using System.Collections.Generic;
using System.Reflection;
using System.IO;

using static LinesGame.sc_Debug;


namespace LinesGame
{
    internal class c_ResourceLoadingException : Exception
    {
        public string pr_sName { get; init; }

        public c_ResourceLoadingException(Exception @base, string name) : base(@base.Message, @base) => pr_sName = name;
    }


    /* resource definition in this part: */
    internal abstract partial class ac_Resource : I_Disposable
    {
#region I_Disposable
        private bool _bIsDisposed = false;

        bool I_Disposable.pr_bIsDisposed { get => _bIsDisposed; set { _bIsDisposed = value; } }
        void I_Disposable.dispose() => disposeResource();
#endregion
        private bool _bIsLoaded = false;
        private string _sName = null!;

        public bool pr_bIsLoaded => _bIsLoaded;
        public string pr_sName {
            get {
                if (_sName is not null)
                    return _sName;
                else {
                    st_Assert(_bIsLoaded == false, "Resource is loaded but name is not set!");
                    return "--<NOT_INITED>--";
                }
            }
            init {
                _sName = value;
            }
        }

        protected abstract void disposeResource();

        ~ac_Resource()
        {
            (this as I_Disposable).Dispose();
        }
    }


    /* Loading interfaces definition (must be nested in Resource to see it's private members): */
    internal abstract partial class ac_Resource
    {
        internal interface I_StreamLoadable
        {
            public static T st_LoadResource<T>(string name, Stream stream)
                where T : notnull, ac_Resource, I_StreamLoadable, new()
            {
                T res = new() { pr_sName = name };
                try {
                    res.loadFromStream(stream);
                }
                catch (Exception ex) {
                    throw new c_ResourceLoadingException(ex, name);
                }
                res._bIsLoaded = true;
                return res;
            }

            protected void loadFromStream(Stream stream);
        }

        internal interface I_ParameterizedStreamLoadable<in T>
        {
            public static OutT st_LoadResource<OutT, ParamsT>(string name, Stream stream, ParamsT @params)
                where OutT : notnull, ac_Resource, I_ParameterizedStreamLoadable<ParamsT>, new()
            {
                OutT res = new() { pr_sName = name };
                try {
                    res.loadFromStreamWithParams(stream, @params);
                }
                catch (Exception ex) {
                    throw new c_ResourceLoadingException(ex, name);
                }
                res._bIsLoaded = true;
                return res;
            }

            protected void loadFromStreamWithParams(Stream stream, T @params);
        }

        internal interface I_SourceLoadable<in SrcT>
            where SrcT : notnull
        {
#pragma warning disable CS0693
            public static OutT st_LoadResource<OutT, SrcT>(string name, SrcT source)
                where OutT : notnull, ac_Resource, I_SourceLoadable<SrcT>, new()
                where SrcT : notnull
            {
                OutT res = new() { pr_sName = name };
                try {
                    res.loadFromSource(source);
                }
                catch (Exception ex) {
                    throw new c_ResourceLoadingException(ex, name);
                }
                res._bIsLoaded = true;
                return res;
            }
#pragma warning restore CS0693

            protected void loadFromSource(SrcT source);
        }

        internal interface I_ParameterizedSourceLoadable<in SrcT, ParamsT>
            where SrcT : notnull
        {
#pragma warning disable CS0693
            public static OutT st_LoadResource<OutT, SrcT>(string name, SrcT source, ParamsT parameters)
                where OutT : notnull, ac_Resource, I_ParameterizedSourceLoadable<SrcT, ParamsT>, new()
                where SrcT : notnull
            {
                OutT res = new() { pr_sName = name };
                try {
                    res.loadFromSourceWithParams(source, parameters);
                }
                catch (Exception ex) {
                    throw new c_ResourceLoadingException(ex, name);
                }
                res._bIsLoaded = true;
                return res;
            }
#pragma warning restore CS0693

            protected void loadFromSourceWithParams(SrcT source, ParamsT @params);
        }
    }


    internal class c_ResourceManager
    {
        private static c_ResourceManager? st_instance = null;
        public static c_ResourceManager st_pr_Instance
        {
            get => st_instance ??= new c_ResourceManager();
        }
        public static bool st_pr_bExists => st_instance != null;
        //--------------------------------------------------------------------------------------------------------------
        private readonly Dictionary<string, ac_Resource> ro_resources;


        public bool IsLoaded(string name) => ro_resources.ContainsKey(name);

        public T? GetResource<T>(string name)
            where T : ac_Resource?, new()
        {
            st_Assert(!string.IsNullOrEmpty(name), "Invalid name");

            if (IsLoaded(name)) {
                var res = ro_resources.GetValueOrDefault(name);

                st_Assert(res != null, $"Resource with name \"{name}\" is null", new NullReferenceException());

                if (res is T res_of_requested_type)
                    return res_of_requested_type;
                else
                    throw new InvalidOperationException($"Resource \"{name}\" has type {res!.GetType().Name} but requested type is {typeof(T).Name}");
            }
            else
                return null;
        }

        public ResT LoadResourceFromAssembly<ResT>(string name, bool tryGet = true)
            where ResT : notnull, ac_Resource, ac_Resource.I_StreamLoadable, new()
        {
            st_Assert(!string.IsNullOrEmpty(name), "Invalid name");

            ResT? resource = null;

            if (tryGet)
                resource = GetResource<ResT>(name);

            if (resource != null) {
                return resource;
            }
            //if this resource is not in cache:
            else {
                st_debugLog(nameof(LoadResourceFromAssembly) + ": " + name);

                try {
                    var stream = st_searchResourceInAssembly(name);
                    resource = ac_Resource.I_StreamLoadable.st_LoadResource<ResT>(name, stream);
                    ro_resources.Add(name, resource);
                    return resource;
                }
                catch (Exception ex) {
                    sc_Logger.st_WriteLog(ex);
                    throw;
                }
            }
        }
        public ResT LoadResourceFromAssembly<ResT, ParamsT>(string name, ParamsT @params, bool tryGet = true)
            where ResT : notnull, ac_Resource, ac_Resource.I_ParameterizedStreamLoadable<ParamsT>, new()
            where ParamsT : notnull
        {
            st_Assert(!string.IsNullOrEmpty(name), "Invalid name");

            ResT? resource = null;

            if (tryGet)
                resource = GetResource<ResT>(name);

            if (resource != null) {
                return resource;
            }
            //if this resource is not in cache:
            else {
                st_debugLog(nameof(LoadResourceFromAssembly) + ": " + name);

                try {
                    var stream = st_searchResourceInAssembly(name);
                    resource = ac_Resource.I_ParameterizedStreamLoadable<ParamsT>.st_LoadResource<ResT, ParamsT>(name, stream, @params);
                    ro_resources.Add(name, resource);
                    return resource;
                }
                catch (Exception ex) {
                    sc_Logger.st_WriteLog(ex);
                    throw;
                }
            }
        }
        public ResT LoadResourceFromSource<ResT, SrcT>(string name, SrcT source, bool tryGet = true)
            where ResT : notnull, ac_Resource, ac_Resource.I_SourceLoadable<SrcT>, new()
            where SrcT : notnull
        {
            st_Assert(!string.IsNullOrEmpty(name), "Invalid name");

            ResT? resource = null;

            if (tryGet)
                resource = GetResource<ResT>(name);

            if (resource != null) {
                return resource;
            }
            //if this resource is not in cache:
            else {
                st_debugLog(nameof(LoadResourceFromSource) + ": " + name);

                try {
                    resource = ac_Resource.I_SourceLoadable<SrcT>.st_LoadResource<ResT, SrcT>(name, source);
                    ro_resources.Add(name, resource);
                    return resource;
                }
                catch(Exception ex) {
                    sc_Logger.st_WriteLog(ex);
                    throw;
                }
            }
        }
        public ResT LoadResourceFromSource<ResT, SrcT, ParamsT>(string name, SrcT source, ParamsT @params, bool tryGet = true)
            where ResT : notnull, ac_Resource, ac_Resource.I_ParameterizedSourceLoadable<SrcT, ParamsT>, new()
            where SrcT : notnull
        {
            st_Assert(!string.IsNullOrEmpty(name), "Invalid name");

            ResT? resource = null;

            if (tryGet)
                resource = GetResource<ResT>(name);

            if (resource != null) {
                return resource;
            }
            //if this resource is not in cache:
            else {
                st_debugLog(nameof(LoadResourceFromSource) + ": " + name);

                try {
                    resource = ac_Resource.I_ParameterizedSourceLoadable<SrcT, ParamsT>.st_LoadResource<ResT, SrcT>(name, source, @params);
                    ro_resources.Add(name, resource);
                    return resource;
                }
                catch (Exception ex) {
                    sc_Logger.st_WriteLog(ex);
                    throw;
                }
            }
        }

        public void AddResource(ac_Resource res, string name)
        {
#region Validate_Arguments
            if (string.IsNullOrEmpty(name))
                throw new ArgumentException("name is null or empty", nameof(name));
            else if (IsLoaded(name))
                throw new InvalidOperationException($"Resource with name \"{name}\" already present in system");

            else if (res == null)
                throw new ArgumentNullException(nameof(res));
            else if (!res.pr_bIsLoaded)
                throw new ArgumentException("Resource is not loaded. Name: " + name, nameof(res));
#endregion

            if (ro_resources.ContainsValue(res)) {
                throw new InvalidOperationException($"Given resource for name \"{name}\" alreaady exists but with name \"{getResourceName(res)}\"");
            }

            ro_resources.Add(name, res);
        }
        public void FreeResource(string name)
        {
            st_Assert(!string.IsNullOrEmpty(name), "Invalid name");

            var res = ro_resources.GetValueOrDefault(name);

            if (res != null) {
                (res as I_Disposable).Dispose();
                ro_resources.Remove(name);
#if DEBUG
                System.Diagnostics.Debug.Print("Dispose resource: " + name);
#endif
            }
            else
                throw new InvalidOperationException($"Resource with name \"{name}\" not found!");
        }
        public void FreeAllResources()
        {
#if DEBUG
            System.Diagnostics.Debug.Print("Free resources: " + ro_resources.Count.ToString());
            int idx = 0;
#endif
            foreach (var entry in ro_resources) {
                (entry.Value as I_Disposable).Dispose();
#if DEBUG
                System.Diagnostics.Debug.Print($"Dispose resource ({idx++}): {entry.Key}");
#endif
            }
            ro_resources.Clear();
        }


        private string? getResourceName(ac_Resource res)
        {
            foreach(var key_value_pair in ro_resources) {
                if (key_value_pair.Value == res) {
                    return key_value_pair.Key;
                }
            }
            return null;
        }


        private static Stream st_searchResourceInAssembly(string name)
        {
            Stream? stream = null;

            var names = Assembly.GetCallingAssembly().GetManifestResourceNames();
            name = "." + name;

            foreach (var manifestResName in names) {
                if (manifestResName.EndsWith(name)) {
                    stream = Assembly.GetCallingAssembly().GetManifestResourceStream(manifestResName);
                }
            }

            if (stream != null)
                return stream;
            else {
                throw new Exception($"Resource \"{name}\" not found!");
            }
        }

        [method: System.Diagnostics.Conditional("DEBUG")]
        private static void st_debugLog(string message)
        {
            System.Diagnostics.Debug.Print(message);
        }


        private c_ResourceManager()
        {
            this.ro_resources = new Dictionary<string, ac_Resource>();
        }
        ~c_ResourceManager()
        {
            FreeAllResources();
        }
    }
}