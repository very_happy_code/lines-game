﻿
namespace LinesGame
{
    partial class c_LeaderboardWnd
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.ListViewItem listViewItem1 = new System.Windows.Forms.ListViewItem(new string[] {
            "Player_1",
            "123456",
            "01:35:40",
            "01.01.2021"}, -1);
            System.Windows.Forms.ListViewItem listViewItem2 = new System.Windows.Forms.ListViewItem(new string[] {
            "LongLongPlayerName1",
            "123456",
            "12:20",
            "01.01.2021"}, -1);
            this.listView = new System.Windows.Forms.ListView();
            this.userNameColumn = new System.Windows.Forms.ColumnHeader();
            this.scoreColumn = new System.Windows.Forms.ColumnHeader();
            this.gameTimeColumn = new System.Windows.Forms.ColumnHeader();
            this.playDateColumn = new System.Windows.Forms.ColumnHeader();
            this.SuspendLayout();
            // 
            // listView
            // 
            this.listView.Alignment = System.Windows.Forms.ListViewAlignment.Default;
            this.listView.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.userNameColumn,
            this.scoreColumn,
            this.gameTimeColumn,
            this.playDateColumn});
            this.listView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.listView.GridLines = true;
            this.listView.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable;
            this.listView.HideSelection = false;
            this.listView.ImeMode = System.Windows.Forms.ImeMode.Off;
            this.listView.Items.AddRange(new System.Windows.Forms.ListViewItem[] {
            listViewItem1,
            listViewItem2});
            this.listView.LabelWrap = false;
            this.listView.Location = new System.Drawing.Point(0, 0);
            this.listView.MultiSelect = false;
            this.listView.Name = "listView";
            this.listView.ShowGroups = false;
            this.listView.Size = new System.Drawing.Size(564, 361);
            this.listView.TabIndex = 0;
            this.listView.UseCompatibleStateImageBehavior = false;
            this.listView.View = System.Windows.Forms.View.Details;
            // 
            // userNameColumn
            // 
            this.userNameColumn.Text = "Name";
            this.userNameColumn.Width = 200;
            // 
            // scoreColumn
            // 
            this.scoreColumn.Text = "Score";
            this.scoreColumn.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.scoreColumn.Width = 100;
            // 
            // gameTimeColumn
            // 
            this.gameTimeColumn.Text = "Time";
            this.gameTimeColumn.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.gameTimeColumn.Width = 120;
            // 
            // playDateColumn
            // 
            this.playDateColumn.Text = "Play Date";
            this.playDateColumn.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.playDateColumn.Width = 140;
            // 
            // c_LeaderboardWnd
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(564, 361);
            this.Controls.Add(this.listView);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.MinimumSize = new System.Drawing.Size(280, 300);
            this.Name = "c_LeaderboardWnd";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.Text = "Leaderboard:";
            this.TopMost = true;
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ListView listView;
        private System.Windows.Forms.ColumnHeader userNameColumn;
        private System.Windows.Forms.ColumnHeader scoreColumn;
        private System.Windows.Forms.ColumnHeader gameTimeColumn;
        private System.Windows.Forms.ColumnHeader playDateColumn;
    }
}