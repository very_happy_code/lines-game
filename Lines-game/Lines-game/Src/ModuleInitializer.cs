﻿#nullable enable

#define UNIT_TESTING

using global::System;
#if DEBUG && !UNIT_TESTING
using System.Runtime.CompilerServices;
using System.Diagnostics;
#endif


namespace LinesGame
{
    public static class sc_ModuleInitializer
    {
#if DEBUG && !UNIT_TESTING
        [method: ModuleInitializer()]
        public static void st_ExeInit()
        {
            Debugger.Launch();

            Exception? exception = st_TestTypesOnIndexationEnumAttributeCondition();

            if (exception != null) {
                Debugger.Break();
                throw exception;
            }

            exception = st_TestTypesOnFactoryUnityAttributeCondition();

            if (exception != null) {
                Debugger.Break();
                throw exception;
            }
        }
#endif

#if UNIT_TESTING
        public static Exception? st_TestTypesOnIndexationEnumAttributeCondition()
        {
            return atr_IndexationEnumAttribute.st_CheckConditionsForAllTypes();
        }
        public static Exception? st_TestTypesOnFactoryUnityAttributeCondition()
        {
            return atr_FactoryUnitAttribute.st_CheckConditionsForAllTypes();
        }
#endif
    }
}