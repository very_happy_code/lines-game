﻿#nullable enable

using global::System;
using System.IO;
using System.Drawing;
using System.Collections.Generic;

using static LinesGame.sc_Debug;

using r_SpriteSheetParams_t = LinesGame.c_GDISpriteSheetResource.r_SpriteSheetParams;
using r_AnimationParams_t = LinesGame.c_GDISpriteAnimationResource.r_SpriteAnimationParams;
using r_ImageFontParams_t = LinesGame.c_GDIImageFontResource.r_ImageFontParams;


namespace LinesGame
{
    internal class c_GDIImageResource : ac_Resource, ac_Resource.I_StreamLoadable
    {
        private Image? _image = null;

        public Image? pr_Image => _image;


        void I_StreamLoadable.loadFromStream(Stream stream)
        {
            _image = Image.FromStream(stream);
        }
        protected override void disposeResource()
        {
            _image?.Dispose();
            _image = null;
        }
    }


    internal class c_GDISpriteSheetResource : ac_Resource, ac_Resource.I_ParameterizedStreamLoadable<r_SpriteSheetParams_t>
    {
        public const int nTAKE_SIZE_FROM_FILE = -1;
        public record r_SpriteSheetParams ( int pr_nMapWidthSprites, int pr_nMapHeightSprites, int pr_nSpriteWidthPx = nTAKE_SIZE_FROM_FILE, int pr_nSpriteHeightPx = nTAKE_SIZE_FROM_FILE, Color? pr_TransparencyMask = null );
        //--------------------------------------------------------------------------------------------------------------

        private Image[,]? spriteMap = null;
        private int _nSpriteWidthPx, _nSpriteHeightPx;
        private int _nMapWidthSprites, _nMapHeightSprites;

        public int pr_nSpriteWidthPx { get => _nSpriteWidthPx; }
        public int pr_nSpriteHeightPx { get => _nSpriteHeightPx; }
        public int pr_nMapWidthSprites { get => _nMapWidthSprites; }
        public int pr_nMapHeightSprites { get => _nMapHeightSprites; }

        public Image this[int x, int y]
        {
            get {
                st_Assert(pr_bIsLoaded, $"Resource {pr_sName} is not loaded!");
                st_Assert(x >= 0 && x < _nMapWidthSprites, $"{pr_sName}: Invalid index x: {x}, wid: {_nMapWidthSprites}", new IndexOutOfRangeException());
                st_Assert(y >= 0 && y < _nMapHeightSprites, $"{pr_sName}: Invalid index x: {y}, hgt: {_nMapHeightSprites}", new IndexOutOfRangeException());

                return spriteMap![x, y];
            }
        }


        protected override void disposeResource()
        {
            spriteMap?.ext_DisposeAll();
            spriteMap = null;
        }

        void I_ParameterizedStreamLoadable<r_SpriteSheetParams_t>.loadFromStreamWithParams(Stream stream, r_SpriteSheetParams_t @params)
        {
            _nSpriteWidthPx = @params.pr_nSpriteWidthPx;
            _nSpriteHeightPx = @params.pr_nSpriteHeightPx;
            _nMapWidthSprites = @params.pr_nMapWidthSprites;
            _nMapHeightSprites = @params.pr_nMapHeightSprites;

            st_Assert(_nMapWidthSprites > 0, pr_sName + " Invalid map width: " + _nMapWidthSprites.ToString());
            st_Assert(_nMapHeightSprites > 0, pr_sName + " Invalid map height: " + _nMapHeightSprites.ToString());

            spriteMap = new Image[_nMapWidthSprites, _nMapHeightSprites];

            using var src = Image.FromStream(stream);

            if (_nSpriteWidthPx == nTAKE_SIZE_FROM_FILE) {
                _nSpriteWidthPx = src.Width / _nMapWidthSprites;
            }
            if (_nSpriteHeightPx == nTAKE_SIZE_FROM_FILE) {
                _nSpriteHeightPx = src.Height / _nMapHeightSprites;
            }

            for (int x = 0; x < _nMapWidthSprites; ++x) {
                for (int y = 0; y < _nMapHeightSprites; ++y) {
                    var bmp = new Bitmap(_nSpriteWidthPx, _nSpriteHeightPx);

                    using (var gr = Graphics.FromImage(bmp)) {
                        gr.DrawImage(
                            image: src,
                            srcRect: new RectangleF(x * _nSpriteWidthPx, y * _nSpriteHeightPx, _nSpriteWidthPx, _nSpriteHeightPx),
                            destRect: new Rectangle(0, 0, _nSpriteWidthPx, _nSpriteHeightPx),
                            srcUnit: GraphicsUnit.Pixel
                            );
                    }

                    if (@params.pr_TransparencyMask != null)
                        sc_GDIUtils.st_ApplyTransparencyMask(bmp, @params.pr_TransparencyMask.Value);

                    spriteMap[x, y] = bmp;
                }
            }

        }
    }


    internal class c_GDIImageFontResource 
        : ac_Resource,
        ac_Resource.I_ParameterizedSourceLoadable<c_GDISpriteSheetResource, char[]>,
        ac_Resource.I_ParameterizedSourceLoadable<c_GDISpriteSheetResource, r_ImageFontParams_t>
    {
        public record r_ImageFontParams( char[] pr_SheetChars, Image[] pr_AdditionalImages, char[] pr_AdditionalChars );
        //--------------------------------------------------------------------------------------------------------------
        private Dictionary<char, Image>? images = null;

        public Image this[char key]
        {
            get {
                st_Assert(pr_bIsLoaded, $"Resource \"{pr_sName}\" is not loaded!", new InvalidOperationException());
                if (images!.TryGetValue(key, out Image? result)) {
                    return result;
                }
                else
                    throw new KeyNotFoundException($"Font \"{pr_sName}\" does not contain key \"{key}\"");
            }
        }

        protected override void disposeResource()
        {
            if (images != null) {
                foreach(var key_value in images) {
                    key_value.Value.Dispose();
                }
                images.Clear();
                images = null;
            }
        }

        void I_ParameterizedSourceLoadable<c_GDISpriteSheetResource, char[]>.loadFromSourceWithParams(c_GDISpriteSheetResource source, char[] @params)
        {
            st_Assert(source.pr_bIsLoaded, $"Source \"{source.pr_sName}\" is not loaded", new InvalidOperationException());
            st_Assert(source.pr_nMapHeightSprites == 1, $"Source have \"{source.pr_sName}\" invalid height: " + source.pr_nMapHeightSprites.ToString(), new InvalidOperationException());
            st_Assert(source.pr_nMapWidthSprites == @params.Length, $"Source \"{source.pr_sName}\" width ({source.pr_nMapWidthSprites}) != @params width ({@params.Length})");

            int count = @params.Length;

            images = new Dictionary<char, Image>(count);

            for (int i = 0; i < count; ++i) {
                images.Add(@params[i], (source[i, y: 0].Clone() as Image)!);
            }
        }

        void I_ParameterizedSourceLoadable<c_GDISpriteSheetResource, r_ImageFontParams_t>.loadFromSourceWithParams(c_GDISpriteSheetResource source, r_ImageFontParams_t @params)
        {
            st_Assert(source.pr_bIsLoaded, $"Source \"{source.pr_sName}\" is not loaded", new InvalidOperationException());
            st_Assert(source.pr_nMapHeightSprites == 1, $"Source \"{source.pr_sName}\" have invalid height: " + source.pr_nMapHeightSprites.ToString(), new InvalidOperationException());
            st_Assert(source.pr_nMapWidthSprites == @params.pr_SheetChars.Length, $"Source \"{source.pr_sName}\" width ({source.pr_nMapWidthSprites}) != @params pr_SheetChars length ({@params.pr_SheetChars.Length})");
            st_Assert(@params.pr_AdditionalChars.Length == @params.pr_AdditionalImages.Length, $"Additional data length inconsistency: chars({@params.pr_AdditionalChars.Length}) != images({@params.pr_AdditionalImages.Length})");

            images = new Dictionary<char, Image>(@params.pr_SheetChars.Length + @params.pr_AdditionalChars.Length);

            for (int i = 0; i < @params.pr_SheetChars.Length; ++i) {
                images.Add(@params.pr_SheetChars[i], (source[i, y: 0].Clone() as Image)!);
            }

            for (int j = 0; j < @params.pr_AdditionalChars.Length; ++j) {
                images.Add(@params.pr_AdditionalChars[j], (@params.pr_AdditionalImages[j].Clone() as Image)!);
            }
        }
    }


    internal class c_GDISpriteAnimationResource : ac_Resource, ac_Resource.I_ParameterizedSourceLoadable<c_GDISpriteSheetResource, r_AnimationParams_t>
    {
        public record r_SpriteAnimationParams( float pr_fLengthSeconds, int pr_nAnimSourceIdx_Y, int pr_nAnimSourceIdx_X, int pr_nAnimLengthFrames );
        //--------------------------------------------------------------------------------------------------------------
        private float _fLengthSeconds = -1.0f;
        private Image[]? frames = null;


        public float pr_fLengthSeconds { get => _fLengthSeconds; }
        public int pr_nLengthFrames { get => frames?.Length ?? -1; }


        public Image this[Index idx]
        {
            get {
                st_Assert(pr_bIsLoaded, "Anim is not loaded!", new InvalidOperationException());
                return frames![!idx.IsFromEnd ? idx.Value : pr_nLengthFrames - idx.Value];
            }
        }
        public Image this[int idx]
        {
            get {
                st_Assert(pr_bIsLoaded, "Anim is not loaded!", new InvalidOperationException());
                return frames![idx];
            }
        }


        public Image GetFrameAt(float timeSeconds, bool inverse)
        {
            st_Assert(pr_bIsLoaded, $"Resource \"{pr_sName}\" is not loaded!", new InvalidOperationException());
            st_Assert(timeSeconds >= .0f && timeSeconds <= _fLengthSeconds, $"\"{pr_sName}\" Invalid time: {timeSeconds}; Length: {_fLengthSeconds}", new ArgumentOutOfRangeException(nameof(timeSeconds)));

            float frame_duration_sec = _fLengthSeconds / ( float )pr_nLengthFrames;
            int frame_idx = ( int )(timeSeconds / frame_duration_sec);

            if (frame_idx == frames!.Length) {
                frame_idx--;
            }

            if (inverse) {
                frame_idx = frames!.Length - frame_idx - 1;
            }

            return frames![frame_idx];
        }


        protected override void disposeResource()
        {
            frames?.ext_DisposeAll();
            frames = null;
        }

        void I_ParameterizedSourceLoadable<c_GDISpriteSheetResource, r_AnimationParams_t>.loadFromSourceWithParams(c_GDISpriteSheetResource source, r_AnimationParams_t @params)
        {
            loadResourceFromSourceWithParams(source, @params);
        }

        //need to make it visible in children to allow extend loading functionality:
        protected void loadResourceFromSourceWithParams(c_GDISpriteSheetResource source, r_AnimationParams_t @params)
        {
            st_Assert(source.pr_bIsLoaded, $"Source \"{source.pr_sName}\" is not loaded!", new ArgumentException("Source is not loaded!", nameof(source)));

            _fLengthSeconds = @params.pr_fLengthSeconds;

            frames = new Image[@params.pr_nAnimLengthFrames];

            for (int i = @params.pr_nAnimSourceIdx_X, j = 0; i < @params.pr_nAnimSourceIdx_X + @params.pr_nAnimLengthFrames; ++i, ++j) {
                frames[j] = (source[i, @params.pr_nAnimSourceIdx_Y].Clone() as Image)!;
            }
        }
    }


    internal class c_StreamResource : ac_Resource, ac_Resource.I_StreamLoadable
    {
        private Stream? _stream = null;


        public Stream? pr_Stream { get => _stream; }


        void I_StreamLoadable.loadFromStream(Stream stream)
        {
            _stream = stream;
        }
        protected override void disposeResource()
        {
            _stream = null;
        }
    }
}