﻿#nullable enable

using global::System;
using System.Drawing;


namespace LinesGame
{
    internal struct s_Animation
    {
        private readonly c_GDISpriteAnimationResource _ro_resource;
        private float _fCurrentTimeSec;

        public bool m_bWrapTime;
        public bool m_bInverseFrames;   //when true, animation plays backward


        public readonly c_GDISpriteAnimationResource pr_Resource { get => _ro_resource; init => _ro_resource = value; }
        public float pr_fCurrentTimeSeconds
        {
            get => _fCurrentTimeSec;
            set {
                if (m_bWrapTime)
                    value = st_wrapTime(value, _ro_resource.pr_fLengthSeconds);
                //else = truncate:
                else {
                    if (value < .0f) {
                        value = .0f;
                    }
                    else if (value > _ro_resource.pr_fLengthSeconds) {
                        value = _ro_resource.pr_fLengthSeconds;
                    }
                }

                _fCurrentTimeSec = value;
            }
        }
        public readonly bool pr_bIsFinished
        {
            get {
                if (!m_bWrapTime) {
                    return _fCurrentTimeSec >= _ro_resource.pr_fLengthSeconds;
                }
                else {
                    return false;
                }
            }
        }


        public readonly Image GetCurrentFrame() => _ro_resource.GetFrameAt(_fCurrentTimeSec, m_bInverseFrames);


        private static float st_wrapTime(float givenTime, float animLength)
        {
            if (givenTime >= .0f && givenTime <= animLength) {
                return givenTime;
            }
            else {
                float v = MathF.Abs(givenTime / animLength);
                v -= ( int )v;
                return v * animLength;
            }
        }
    }
}