﻿#nullable enable

using global::System;
using System.Collections.Generic;

using Iterator_t = System.Collections.Generic.IEnumerator<LinesGame.I_CoroutineBehavior>;


namespace LinesGame
{
    internal enum en_CoroutineBehavior : byte
    {
        eWaitForNextFrame = 0x00,
        eWaitForSeconds
    }


    internal interface I_CoroutineBehavior
    {
        public en_CoroutineBehavior pr_IntendedBehavior { get; }
    }


    internal struct s_CoroutineWaitForSeconds : I_CoroutineBehavior
    {
        en_CoroutineBehavior I_CoroutineBehavior.pr_IntendedBehavior => en_CoroutineBehavior.eWaitForSeconds;

        public readonly float pr_fDelaySeconds { get; init; }

        public s_CoroutineWaitForSeconds(float delaySeconds)
        {
            this.pr_fDelaySeconds = delaySeconds;
        }
    }


    internal struct s_CoroutineWaitForNextFrame : I_CoroutineBehavior
    {
        readonly en_CoroutineBehavior I_CoroutineBehavior.pr_IntendedBehavior => en_CoroutineBehavior.eWaitForNextFrame;
    }


    internal delegate IEnumerable<I_CoroutineBehavior> dl_CoroutineFunc();


    internal sealed partial record ser_Coroutine : I_Disposable
    {
        private readonly Iterator_t ro_activeIterator;
        private bool _bIsExecuting = false;


        public uint pr_unID { get; private init; }
        public bool pr_bIsExecuting { get => _bIsExecuting; }
        bool I_Disposable.pr_bIsDisposed { get; set; }


        public void Restart()
        {
            sc_CoroutineManager.st_RestartCoroutine(this);
        }
        public void Stop()
        {
            sc_CoroutineManager.st_StopCoroutine(this);
        }

        public bool Equals(ser_Coroutine? handle)
        {
            if (handle == null)
                return false;
            else
                return handle.pr_unID == this.pr_unID;
        }
        public override int GetHashCode() => ( int )pr_unID;


        void I_Disposable.dispose()
        {
            ro_activeIterator.Dispose();
        }


        public static ser_Coroutine st_Start(dl_CoroutineFunc func)
        {
            return sc_CoroutineManager.st_StartCoroutine(func);
        }
        public static ser_Coroutine st_Start(IEnumerable<I_CoroutineBehavior> coroutine)
        {
            return sc_CoroutineManager.st_StartCoroutine(coroutine);
        }


        private ser_Coroutine(Iterator_t it)
        {
            this.ro_activeIterator = it;
        }
        ~ser_Coroutine()
        {
            (this as I_Disposable).Dispose();
        }
    }
}