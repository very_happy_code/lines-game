﻿#nullable enable


using global::System;


namespace LinesGame
{
    internal static class sc_LinesDirectoriesHub
    {
        public const string sSAVE_DIR = "Saves";
        public const string sSAVE_FILE = "save.sav";

        public const string sLEADERBOARD_DIR = sDATA_DIR;
        public const string sLEADERBOARD_FILE = "leaderboard.bin";

        public const string sLOGS_DIR = "Logs";

        public const string sUSER_STATISTICS_DIR = sDATA_DIR;
        public const string sUSER_STATISTICS_FILE = "GlobalStats.xml";


        private const string sDATA_DIR = "Data";
    }
}