﻿
namespace LinesGame
{
    partial class c_GameWnd
    {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        ///  Required method for Designer support - do not modify
        ///  the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(c_GameWnd));
            this.pbFieldDisplay = new System.Windows.Forms.PictureBox();
            this.msMenu = new System.Windows.Forms.MenuStrip();
            this.tsmiGame = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiGame_CancelLastTurn = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiGame_NewGame = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiGame_ShowLeaderboard = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator_Game = new System.Windows.Forms.ToolStripSeparator();
            this.tsmiGame_Exit = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiAbout = new System.Windows.Forms.ToolStripMenuItem();
            this.pbPanelBackground = new System.Windows.Forms.PictureBox();
            this.pbScoreDisplay = new System.Windows.Forms.PictureBox();
            this.pbTimeDisplay = new System.Windows.Forms.PictureBox();
            this.pbPredictionDisplay = new System.Windows.Forms.PictureBox();
            this.lblScore = new System.Windows.Forms.Label();
            this.lblTime = new System.Windows.Forms.Label();
            this.pbBestScoreDisplay = new System.Windows.Forms.PictureBox();
            this.lblBestScore = new System.Windows.Forms.Label();
            this.pnUIPanel = new System.Windows.Forms.Panel();
            ((System.ComponentModel.ISupportInitialize)(this.pbFieldDisplay)).BeginInit();
            this.msMenu.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbPanelBackground)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbScoreDisplay)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbTimeDisplay)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbPredictionDisplay)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbBestScoreDisplay)).BeginInit();
            this.pnUIPanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // pbFieldDisplay
            // 
            this.pbFieldDisplay.BackColor = System.Drawing.SystemColors.Control;
            this.pbFieldDisplay.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.pbFieldDisplay.Location = new System.Drawing.Point(0, 76);
            this.pbFieldDisplay.Margin = new System.Windows.Forms.Padding(0);
            this.pbFieldDisplay.Name = "pbFieldDisplay";
            this.pbFieldDisplay.Size = new System.Drawing.Size(410, 410);
            this.pbFieldDisplay.TabIndex = 0;
            this.pbFieldDisplay.TabStop = false;
            // 
            // msMenu
            // 
            this.msMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsmiGame,
            this.tsmiAbout});
            this.msMenu.Location = new System.Drawing.Point(0, 0);
            this.msMenu.Name = "msMenu";
            this.msMenu.Size = new System.Drawing.Size(410, 24);
            this.msMenu.TabIndex = 5;
            this.msMenu.Text = "Main menu";
            // 
            // tsmiGame
            // 
            this.tsmiGame.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsmiGame_CancelLastTurn,
            this.tsmiGame_NewGame,
            this.tsmiGame_ShowLeaderboard,
            this.toolStripSeparator_Game,
            this.tsmiGame_Exit});
            this.tsmiGame.Name = "tsmiGame";
            this.tsmiGame.Size = new System.Drawing.Size(50, 20);
            this.tsmiGame.Text = "Game";
            // 
            // tsmiGame_CancelLastTurn
            // 
            this.tsmiGame_CancelLastTurn.Name = "tsmiGame_CancelLastTurn";
            this.tsmiGame_CancelLastTurn.Size = new System.Drawing.Size(172, 22);
            this.tsmiGame_CancelLastTurn.Text = "Cancel Last Turn";
            this.tsmiGame_CancelLastTurn.ToolTipText = "Return to previous turn";
            // 
            // tsmiGame_NewGame
            // 
            this.tsmiGame_NewGame.Name = "tsmiGame_NewGame";
            this.tsmiGame_NewGame.Size = new System.Drawing.Size(172, 22);
            this.tsmiGame_NewGame.Text = "New Game";
            this.tsmiGame_NewGame.ToolTipText = "Start new game";
            // 
            // tsmiGame_ShowLeaderboard
            // 
            this.tsmiGame_ShowLeaderboard.Name = "tsmiGame_ShowLeaderboard";
            this.tsmiGame_ShowLeaderboard.Size = new System.Drawing.Size(172, 22);
            this.tsmiGame_ShowLeaderboard.Text = "Show Leaderboard";
            // 
            // toolStripSeparator_Game
            // 
            this.toolStripSeparator_Game.Name = "toolStripSeparator_Game";
            this.toolStripSeparator_Game.Size = new System.Drawing.Size(169, 6);
            // 
            // tsmiGame_Exit
            // 
            this.tsmiGame_Exit.Name = "tsmiGame_Exit";
            this.tsmiGame_Exit.Size = new System.Drawing.Size(172, 22);
            this.tsmiGame_Exit.Text = "Exit";
            // 
            // tsmiAbout
            // 
            this.tsmiAbout.Name = "tsmiAbout";
            this.tsmiAbout.Size = new System.Drawing.Size(52, 20);
            this.tsmiAbout.Text = "About";
            // 
            // pbPanelBackground
            // 
            this.pbPanelBackground.BackColor = System.Drawing.Color.Maroon;
            this.pbPanelBackground.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pbPanelBackground.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pbPanelBackground.Location = new System.Drawing.Point(0, 0);
            this.pbPanelBackground.Margin = new System.Windows.Forms.Padding(0);
            this.pbPanelBackground.Name = "pbPanelBackground";
            this.pbPanelBackground.Size = new System.Drawing.Size(410, 50);
            this.pbPanelBackground.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbPanelBackground.TabIndex = 3;
            this.pbPanelBackground.TabStop = false;
            // 
            // pbScoreDisplay
            // 
            this.pbScoreDisplay.BackColor = System.Drawing.SystemColors.Control;
            this.pbScoreDisplay.Location = new System.Drawing.Point(267, 7);
            this.pbScoreDisplay.Margin = new System.Windows.Forms.Padding(3, 3, 3, 15);
            this.pbScoreDisplay.Name = "pbScoreDisplay";
            this.pbScoreDisplay.Padding = new System.Windows.Forms.Padding(10);
            this.pbScoreDisplay.Size = new System.Drawing.Size(123, 35);
            this.pbScoreDisplay.TabIndex = 0;
            this.pbScoreDisplay.TabStop = false;
            // 
            // pbTimeDisplay
            // 
            this.pbTimeDisplay.BackColor = System.Drawing.SystemColors.Control;
            this.pbTimeDisplay.Location = new System.Drawing.Point(175, 33);
            this.pbTimeDisplay.Name = "pbTimeDisplay";
            this.pbTimeDisplay.Size = new System.Drawing.Size(59, 13);
            this.pbTimeDisplay.TabIndex = 0;
            this.pbTimeDisplay.TabStop = false;
            // 
            // pbPredictionDisplay
            // 
            this.pbPredictionDisplay.BackColor = System.Drawing.SystemColors.Control;
            this.pbPredictionDisplay.Location = new System.Drawing.Point(168, 3);
            this.pbPredictionDisplay.Name = "pbPredictionDisplay";
            this.pbPredictionDisplay.Size = new System.Drawing.Size(75, 25);
            this.pbPredictionDisplay.TabIndex = 0;
            this.pbPredictionDisplay.TabStop = false;
            // 
            // lblScore
            // 
            this.lblScore.AutoSize = true;
            this.lblScore.BackColor = System.Drawing.SystemColors.Control;
            this.lblScore.Location = new System.Drawing.Point(332, 13);
            this.lblScore.Name = "lblScore";
            this.lblScore.Size = new System.Drawing.Size(66, 15);
            this.lblScore.TabIndex = 2;
            this.lblScore.Text = "Score: 0000";
            // 
            // lblTime
            // 
            this.lblTime.AutoSize = true;
            this.lblTime.BackColor = System.Drawing.SystemColors.Control;
            this.lblTime.Location = new System.Drawing.Point(317, 28);
            this.lblTime.Name = "lblTime";
            this.lblTime.Size = new System.Drawing.Size(81, 15);
            this.lblTime.TabIndex = 2;
            this.lblTime.Text = "Time: 00.00.00";
            // 
            // pbBestScoreDisplay
            // 
            this.pbBestScoreDisplay.BackColor = System.Drawing.SystemColors.Control;
            this.pbBestScoreDisplay.Location = new System.Drawing.Point(20, 7);
            this.pbBestScoreDisplay.Name = "pbBestScoreDisplay";
            this.pbBestScoreDisplay.Padding = new System.Windows.Forms.Padding(3);
            this.pbBestScoreDisplay.Size = new System.Drawing.Size(123, 35);
            this.pbBestScoreDisplay.TabIndex = 0;
            this.pbBestScoreDisplay.TabStop = false;
            // 
            // lblBestScore
            // 
            this.lblBestScore.AutoSize = true;
            this.lblBestScore.BackColor = System.Drawing.SystemColors.Control;
            this.lblBestScore.Location = new System.Drawing.Point(12, 18);
            this.lblBestScore.Name = "lblBestScore";
            this.lblBestScore.Size = new System.Drawing.Size(74, 15);
            this.lblBestScore.TabIndex = 2;
            this.lblBestScore.Text = "<BestScore>";
            // 
            // pnUIPanel
            // 
            this.pnUIPanel.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.pnUIPanel.Controls.Add(this.lblBestScore);
            this.pnUIPanel.Controls.Add(this.pbBestScoreDisplay);
            this.pnUIPanel.Controls.Add(this.lblTime);
            this.pnUIPanel.Controls.Add(this.lblScore);
            this.pnUIPanel.Controls.Add(this.pbPredictionDisplay);
            this.pnUIPanel.Controls.Add(this.pbTimeDisplay);
            this.pnUIPanel.Controls.Add(this.pbScoreDisplay);
            this.pnUIPanel.Controls.Add(this.pbPanelBackground);
            this.pnUIPanel.Location = new System.Drawing.Point(0, 25);
            this.pnUIPanel.Name = "pnUIPanel";
            this.pnUIPanel.Size = new System.Drawing.Size(410, 50);
            this.pnUIPanel.TabIndex = 6;
            // 
            // c_GameWnd
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.ClientSize = new System.Drawing.Size(410, 486);
            this.Controls.Add(this.pnUIPanel);
            this.Controls.Add(this.msMenu);
            this.Controls.Add(this.pbFieldDisplay);
            this.DoubleBuffered = true;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MainMenuStrip = this.msMenu;
            this.MaximizeBox = false;
            this.MinimumSize = new System.Drawing.Size(426, 525);
            this.Name = "c_GameWnd";
            this.Text = "Lines";
            ((System.ComponentModel.ISupportInitialize)(this.pbFieldDisplay)).EndInit();
            this.msMenu.ResumeLayout(false);
            this.msMenu.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbPanelBackground)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbScoreDisplay)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbTimeDisplay)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbPredictionDisplay)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbBestScoreDisplay)).EndInit();
            this.pnUIPanel.ResumeLayout(false);
            this.pnUIPanel.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox pbFieldDisplay;
        private System.Windows.Forms.MenuStrip msMenu;
        private System.Windows.Forms.ToolStripMenuItem tsmiGame;
        private System.Windows.Forms.ToolStripMenuItem tsmiGame_CancelLastTurn;
        private System.Windows.Forms.ToolStripMenuItem tsmiGame_NewGame;
        private System.Windows.Forms.ToolStripMenuItem tsmiGame_ShowLeaderboard;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator_Game;
        private System.Windows.Forms.ToolStripMenuItem tsmiGame_Exit;
        private System.Windows.Forms.ToolStripMenuItem tsmiAbout;
        private System.Windows.Forms.PictureBox pbPanelBackground;
        private System.Windows.Forms.PictureBox pbScoreDisplay;
        private System.Windows.Forms.PictureBox pbTimeDisplay;
        private System.Windows.Forms.PictureBox pbPredictionDisplay;
        private System.Windows.Forms.Label lblScore;
        private System.Windows.Forms.Label lblTime;
        private System.Windows.Forms.PictureBox pbBestScoreDisplay;
        private System.Windows.Forms.Label lblBestScore;
        private System.Windows.Forms.Panel pnUIPanel;
    }
}

