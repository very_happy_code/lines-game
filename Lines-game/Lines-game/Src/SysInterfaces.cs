﻿#nullable enable

using global::System;
using System.Collections.Generic;


namespace LinesGame
{
    internal interface I_Disposable : IDisposable
    {
        public bool pr_bIsDisposed { get; protected set; }

        void IDisposable.Dispose()  //override
        {
            if (!pr_bIsDisposed) {
                pr_bIsDisposed = true;
                dispose();
            }
        }

        protected void dispose();
    }


    internal interface I_EnumeratorRef<T> : IEnumerator<T>
    {
        new public ref readonly T Current { get; }
    }


    internal interface I_EnumerableRef<T> : IEnumerable<T>
    {
        new public I_EnumeratorRef<T> GetEnumerator();
    }
}