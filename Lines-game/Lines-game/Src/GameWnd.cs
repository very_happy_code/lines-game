﻿#nullable enable

#undef PROCEDURAL_DRAWING

using global::System;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Windows.Forms;
using System.Reflection;

using static LinesGame.sc_Debug;


namespace LinesGame
{
    internal partial class c_GameWnd : Form
    {
        public const string sFORM_HEADER = "Lines";

        private const int nREDRAW_TIMER_INTERVAL_MILLISECONDS = 20;
        private const int nSCORE_CHAR_COUNT = 6;
        private const float fUI_VERTICAL_PADDING_PX = 1.0f; //padding between field and panel also between panel and menu strip
        private const float fPATH_PEN_WIDTH = 5.0f;
        private const int nUI_PREDICTION_DISPLAY_TOP_PADDING_PX = 3;
        private const int nUI_TIME_DISPLAY_TOP_PADDING_PX = 33;
        private const float fGAP_BETWEEN_SCORE_CHARS_PX = 3.0f;
        private const float fGAP_BETWEEN_TIME_CHARS_PX = 1.0f;
#if PROCEDURAL_DRAWING
        private const float fCELL_BORDER_WIDTH = 3.0f;
#else
        private const float fMOVING_BALL_SIZE_FACTOR = 1.2f;    //we want to scale image of moving ball
#endif

#if PROCEDURAL_DRAWING
        private readonly Pen ro_cellBorderPen;
        private readonly SolidBrush ro_ballsBrush;
#endif
        private readonly Pen ro_pathLinePen;
        private readonly Timer ro_wndUpdatingTimer;
        private readonly string ro_sScoreFormat = new ('0', nSCORE_CHAR_COUNT);
        private PointF[]? pathPoints = null;    //for drawing ball moving path
        private Size? clientSizeOnLatestLayout = null;


        protected override void OnFormClosing(FormClosingEventArgs e)
        {
            var result = DialogResult.OK;

            if (e.CloseReason == CloseReason.UserClosing)
                result = MessageBox.Show("Quit?", sFORM_HEADER, MessageBoxButtons.OKCancel, MessageBoxIcon.Question);

            if (result == DialogResult.Cancel) {
                e.Cancel = true;
                base.OnFormClosing(e);
                return;
            }
            else {
                var user_settings = Properties.Settings.Default;
                user_settings.WindowSize_Width = ClientSize.Width;
                user_settings.WindowSize_Height = ClientSize.Height;
                user_settings.Save();

                sc_Game.st_Save(out string? error_message);

                if (error_message != null) {
                    MessageBox.Show("Can't save user data. Error: " + error_message, sFORM_HEADER, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }

                sc_Game.st_Stop();
                ro_wndUpdatingTimer.Stop();
                base.OnFormClosing(e);
            }
        }

        protected override void OnKeyDown(KeyEventArgs e)
        {
#if DEBUG
            void get_clicked_cell(Point clientPoint, out int x, out int y)
            {
                calculateCellSizeInPixels(out float cell_width_pix, out float cell_height_pix);

                x = ( int )(clientPoint.X / cell_width_pix);
                y = ( int )(clientPoint.Y / cell_height_pix);
            }
            //----------------------------------------------------------------------------------------------------------

            if (e.KeyCode is >= Keys.D1 and <= Keys.D9) {

                //if click point is inside the PbFieldDisplay:
                var point = pbFieldDisplay.PointToClient(Cursor.Position);
                if (pbFieldDisplay.ClientRectangle.Contains(point)) {

                    int key_index = e.KeyCode - Keys.D1;
                    var values = Enum.GetValues<en_BallColors>();

                    if (key_index < values.Length) {
                        get_clicked_cell(point, out int x, out int y);
                        sc_Game.st_DEBUG_SpawnBallAt(x, y, values[key_index]);
                    }
                }

            }
            else if (e.KeyCode == Keys.T) {
                TimeSpan ts = new(23, 59, 45);
                sc_Game.st_DEBUG_SetGameTime(( float )ts.TotalSeconds);
            }
            else if (e.KeyCode == Keys.C) {
                //if click point is inside the PbFieldDisplay:
                var point = pbFieldDisplay.PointToClient(Cursor.Position);
                if (pbFieldDisplay.ClientRectangle.Contains(point)) {
                    get_clicked_cell(point, out int x, out int y);
                    sc_Game.st_DEBUG_ClearCell(x, y);
                }
            }
            else if (e.KeyCode == Keys.R) {
                sc_Game.st_DEBUG_FillWithRandomBalls();
            }
            else if (e.KeyCode == Keys.E) {
                throw new Exception("This is a test exception (in UI thread)!");
            }
            else if (e.KeyCode == Keys.X) {
                sc_Game.st_m_vl_bDEBUG_RaiceException = true;
            }
            else if (e.KeyCode == Keys.P) {
                var sz = this.ClientSize;
                sz.Width *= 2;
                sz.Height *= 2;
                this.ClientSize = sz;
            }
#endif
            base.OnKeyDown(e);
        }

        protected override void OnResizeBegin(EventArgs e)
        {
            base.OnResizeBegin(e);
            SuspendLayout();
        }
        protected override void OnResizeEnd(EventArgs e)
        {
            base.OnResizeEnd(e);

#if DEBUG
            System.Diagnostics.Debug.Print("Resize!");
#endif

            setCorrectClientSize();

            //layout will be performed only if there were actual size changes:
            ResumeLayout(performLayout: true);
        }
        protected override void OnLayout(LayoutEventArgs levent)
        {
            base.OnLayout(levent);

            clientSizeOnLatestLayout = ClientSize;

            //Layout controls:
            var field_native_size = ( SizeF )sc_LinesResourcesProvider.st_GetFieldBackground().pr_Image!.Size;
            float size_factor = ClientSize.Width / field_native_size.Width;

            /* panel: */
            var panel_native_size = ( SizeF )sc_LinesResourcesProvider.st_GetPanelBackground().pr_Image!.Size;
            pnUIPanel.SetBounds(x: 0, y: msMenu.Bottom + ( int )fUI_VERTICAL_PADDING_PX, width: ClientSize.Width, height: ( int )(panel_native_size.Height * size_factor));

            /* field: */
            pbFieldDisplay.SetBounds(x: 0, y: pnUIPanel.Bottom + ( int )fUI_VERTICAL_PADDING_PX, width: ClientSize.Width, height: ClientSize.Width);  //width == height (square field)

            /* prediction panel: */
            var prediction_native_size = ( SizeF )sc_LinesResourcesProvider.st_GetHintImage(en_BallColors.eYellow).Size;
            prediction_native_size.Width *= sc_Game.nBALLS_TO_SPAWN;
            var prediction_size = prediction_native_size * size_factor;
            pbPredictionDisplay.ClientSize = prediction_size.ToSize();
            pbPredictionDisplay.SetBounds(
                x: ( int )(ClientSize.Width / 2 - prediction_size.Width / 2),
                y: ( int )(nUI_PREDICTION_DISPLAY_TOP_PADDING_PX * size_factor),
                width: ( int )prediction_size.Width,
                height: ( int )prediction_size.Height);

            /* BestScore: */
            var score_native_size = ( SizeF )sc_LinesResourcesProvider.st_GetScoreFont()['0'].Size;
            score_native_size.Width *= nSCORE_CHAR_COUNT;
            score_native_size.Width += (nSCORE_CHAR_COUNT - 1) * fGAP_BETWEEN_SCORE_CHARS_PX;
            var score_size = (score_native_size * size_factor).ToSize();
            pbBestScoreDisplay.SetBounds(
                x: pbPredictionDisplay.Location.X / 2 - score_size.Width / 2,
                y: pnUIPanel.Height / 2 - score_size.Height / 2,
                width: score_size.Width,
                height: score_size.Height);

            /* Score: */
            pbScoreDisplay.SetBounds(x: pnUIPanel.Width - score_size.Width - pbBestScoreDisplay.Left, y: pbBestScoreDisplay.Location.Y, width: score_size.Width, height: score_size.Height);

            /* Time: */
            var time_native_size = ( SizeF )sc_LinesResourcesProvider.st_GetTimeFont()['0'].Size;
            time_native_size.Width *= 6;
            time_native_size.Width += fGAP_BETWEEN_TIME_CHARS_PX * 7;
            time_native_size.Width += sc_LinesResourcesProvider.st_GetTimeFont()[sc_LinesResourcesProvider.cTIME_DELEMITTER].Size.Width * 2;
            var time_size = time_native_size * size_factor;
            pbTimeDisplay.SetBounds(
                x: pnUIPanel.Width / 2 - ( int )time_size.Width / 2,
                y: ( int )(nUI_TIME_DISPLAY_TOP_PADDING_PX * size_factor),
                width: ( int )time_size.Width,
                height: ( int )time_size.Height);
#if DEBUG
            System.Diagnostics.Debug.Print("Layout!");
#endif
        }


        public c_GameWnd()
        {
            InitializeComponent();

            Text = sFORM_HEADER;

#if DEBUG
            Text += " (DEBUG)";

            var attrs = (Assembly.GetExecutingAssembly().GetCustomAttributes(typeof(AssemblyInformationalVersionAttribute), true) as AssemblyInformationalVersionAttribute[])!;
            Text += " V: " + attrs[0].InformationalVersion;
#else
            var assembly_name = Assembly.GetExecutingAssembly().GetName();
            Text += ' ' + assembly_name.Version?.ToString() ?? string.Empty;
#endif

            pbPanelBackground.Image = sc_LinesResourcesProvider.st_GetPanelBackground().pr_Image;

            pbFieldDisplay.Paint += pbFieldDisplay_Paint;
            pbFieldDisplay.MouseClick += pbFieldDisplay_MouseClick;

            pbPredictionDisplay.Paint += pbPredictionDisplay_Paint;
            pbPredictionDisplay.BackColor = Color.Transparent;

            pbScoreDisplay.Paint += pbScoreDisplay_Paint;
            pbScoreDisplay.BackColor = Color.Transparent;

            pbBestScoreDisplay.Paint += pbBestScoreDisplay_Paint;
            pbBestScoreDisplay.BackColor = Color.Transparent;

            pbTimeDisplay.Paint += pbTimeDisplay_Paint;
            pbTimeDisplay.BackColor = Color.Transparent;

            tsmiGame_NewGame.Click += tsmiGame_NewGame_Click;
            tsmiGame_CancelLastTurn.Click += tsmiGame_CancelLastTurn_Click;
            tsmiGame_Exit.Click += tsmiGame_Exit_Click;
            tsmiGame_ShowLeaderboard.Click += tsmiGame_ShowLeaderbaord_Click;

            tsmiAbout.Click += tsmiAbout_Click;

#if PROCEDURAL_DRAWING
            this.ro_cellBorderPen = new Pen(Color.DarkGray, fCELL_BORDER_WIDTH);
            this.ro_ballsBrush = new SolidBrush(default);
#endif

            var path_brush = new SolidBrush(Color.FromArgb(150, 70, 70, 70));
            this.ro_pathLinePen = new Pen(path_brush);
            ro_pathLinePen.StartCap = LineCap.RoundAnchor;
            ro_pathLinePen.DashStyle = DashStyle.Dash;
            ro_pathLinePen.EndCap = LineCap.ArrowAnchor;
            ro_pathLinePen.Width = fPATH_PEN_WIDTH;
            ro_pathLinePen.Alignment = PenAlignment.Center;
            ro_pathLinePen.LineJoin = LineJoin.Round;

            sc_Game.st_ev_GameOver += onGameOver;
            sc_Game.st_ev_TriggerSound += st_onTriggerSound;

            this.ro_wndUpdatingTimer = new Timer { Interval = nREDRAW_TIMER_INTERVAL_MILLISECONDS };
            ro_wndUpdatingTimer.Tick += wndUpdatingTimer_Tick;
            ro_wndUpdatingTimer.Start();

#if PROCEDURAL_DRAWING
            lblScore.Visible = lblBestScore.Visible = lblTime.Visible = true;
#else
            lblScore.Visible = lblBestScore.Visible = lblTime.Visible = false;
#endif
            
            PerformLayout();    //we need to make shure that OnLayout function will be called

            ClientSize = new Size(Properties.Settings.Default.WindowSize_Width, Properties.Settings.Default.WindowSize_Height);
            setCorrectClientSize();
        }

        ~c_GameWnd()
        {
            pbFieldDisplay.Paint -= pbFieldDisplay_Paint;
            pbFieldDisplay.MouseClick -= pbFieldDisplay_MouseClick;

            pbPredictionDisplay.Paint -= pbPredictionDisplay_Paint;

            pbScoreDisplay.Paint -= pbScoreDisplay_Paint;

            pbBestScoreDisplay.Paint -= pbBestScoreDisplay_Paint;

            pbTimeDisplay.Paint -= pbTimeDisplay_Paint;

            tsmiGame_NewGame.Click -= tsmiGame_NewGame_Click;
            tsmiGame_CancelLastTurn.Click -= tsmiGame_CancelLastTurn_Click;
            tsmiGame_Exit.Click -= tsmiGame_Exit_Click;
            tsmiGame_ShowLeaderboard.Click -= tsmiGame_ShowLeaderbaord_Click;

            tsmiAbout.Click -= tsmiAbout_Click;

            sc_Game.st_ev_GameOver -= onGameOver;
            sc_Game.st_ev_TriggerSound -= st_onTriggerSound;

#if PROCEDURAL_DRAWING
            ro_cellBorderPen.Dispose();
            ro_ballsBrush.Dispose();
#endif

            ro_pathLinePen.Brush.Dispose();
            ro_pathLinePen.Dispose();

            ro_wndUpdatingTimer.Tick -= wndUpdatingTimer_Tick;
            ro_wndUpdatingTimer.Dispose();
        }


        private void tsmiGame_NewGame_Click(object? sender, EventArgs e)
        {
            var result = MessageBox.Show("Restart?", sFORM_HEADER, MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            if (result == DialogResult.Yes) {
                sc_Game.st_Restart();
            }
        }
        private void tsmiGame_CancelLastTurn_Click(object? sender, EventArgs e)
        {
            sc_Game.st_CancelLastTurn();
        }
        private void tsmiGame_Exit_Click(object? sender, EventArgs e)
        {
            this.Close();
        }
        private void tsmiGame_ShowLeaderbaord_Click(object? sender, EventArgs e)
        {
            c_LeaderboardWnd wnd = new (sc_Game.st_m_ro_Leaderboard);
            wnd.ShowDialog(owner: this);
        }
        private void tsmiAbout_Click(object? sender, EventArgs e)
        {
            c_AboutWnd wnd = new();
            wnd.ShowDialog(owner: this);
        }

        private void pbPredictionDisplay_Paint(object sender, PaintEventArgs e)
        {
            var gr = e.Graphics;
            var colors = sc_Game.st_GetNextSpawnColors();

            float cell_width = pbPredictionDisplay.ClientRectangle.Width / ( float )sc_Game.nBALLS_TO_SPAWN;
            float cell_height = pbPredictionDisplay.ClientSize.Height;

#if PROCEDURAL_DRAWING
            gr.Clear(Color.LightGray);
#endif

            for (int i = 0; i < sc_Game.nBALLS_TO_SPAWN; ++i) {
#if PROCEDURAL_DRAWING
                gr.DrawRectangle(ro_cellBorderPen, i * cell_width, 0, cell_width, cell_height);
                ro_ballsBrush.Color = st_switchColor(colors[i]);
                gr.FillEllipse(ro_ballsBrush, i * cell_width, 0, cell_width, cell_height);
#else
                gr.DrawImage(sc_LinesResourcesProvider.st_GetHintImage(colors[i]), i * cell_width, 0, cell_width, cell_height);
#endif
            }
        }
        private void pbScoreDisplay_Paint(object sender, PaintEventArgs e)
        {
#if !PROCEDURAL_DRAWING
            string score = sc_Game.st_pr_unScore.ToString(ro_sScoreFormat);
            st_drawScore(pbScoreDisplay.ClientSize, e.Graphics, score);
#endif
        }
        private void pbBestScoreDisplay_Paint(object sender, PaintEventArgs e)
        {
#if !PROCEDURAL_DRAWING
            uint score = 0U;
            if (sc_Game.st_m_ro_Leaderboard.pr_nEntriesNumber > 0) {
                ref readonly var best = ref sc_Game.st_m_ro_Leaderboard.GetEntryRRef(idx: 0);
                score = best.m_ro_unScore;
            }
            st_drawScore(pbBestScoreDisplay.ClientSize, e.Graphics, score.ToString(ro_sScoreFormat));
#endif
        }
        private void pbTimeDisplay_Paint(object sender, PaintEventArgs e)
        {
#if !PROCEDURAL_DRAWING
            /* In this code we assume that ':' character is present in font and we use this char as devider */
            var gr = e.Graphics;

            TimeSpan ts = new(0, 0, ( int )sc_Game.st_pr_fGameTimeSeconds);
            string time = ts.ToString(@"hh\:mm\:ss");

            var font = sc_LinesResourcesProvider.st_GetTimeFont();

            float char_height = pbTimeDisplay.ClientSize.Height;
            float width_factor = (pbTimeDisplay.ClientSize.Width - fGAP_BETWEEN_TIME_CHARS_PX * 7) / (6f * font['0'].Width + 2f * font[sc_LinesResourcesProvider.cTIME_DELEMITTER].Width);
            float x = .0f;

            for (int i = 0; i < time.Length; ++i) {
                var image = font[time[i]];
                float wid = image.Width * width_factor;
                gr.DrawImage(image, x, 0, wid, char_height);
                x += wid + fGAP_BETWEEN_TIME_CHARS_PX;
            }
#endif
        }
        private void pbFieldDisplay_Paint(object sender, PaintEventArgs e)
        {
#if !PROCEDURAL_DRAWING
            static void calculate_ball_drawing_pos(out float drawX, out float drawY, float stretch_x, float stretch_y, float posX, float posY)
            {
                drawX = (sc_LinesResourcesProvider.fFIELD_BORDER_OFFSET_PX * stretch_x) +       //starting offset +
                        (sc_LinesResourcesProvider.fFIELD_CELL_SIZE_PX * posX * stretch_x) +    //cell_size * pos_x +
                        (sc_LinesResourcesProvider.fGAP_BETWEEN_CELLS_PX * posX * stretch_x);   //gap * count of gaps

                drawY = (sc_LinesResourcesProvider.fFIELD_BORDER_OFFSET_PX * stretch_y) +
                        (posY * sc_LinesResourcesProvider.fFIELD_CELL_SIZE_PX * stretch_y) +
                        (posY * sc_LinesResourcesProvider.fGAP_BETWEEN_CELLS_PX * stretch_y);
            }
#endif
            //----------------------------------------------------------------------------------------------------------
            var gr = e.Graphics;

            calculateCellSizeInPixels(out float cell_width_pix, out float cell_height_pix);
#if !PROCEDURAL_DRAWING
            calculateStretchFactor(out float stretch_x, out float stretch_y);
#endif

            /* draw field (cells): */
            #region Draw Field
#if PROCEDURAL_DRAWING
            gr.Clear(Color.LightGray);  //background

            //vertical lines:
            for (int x = 0; x < sc_Game.nFIELD_WIDTH_CELLS + 1; ++x) {
                float draw_x = x * cell_width_pix;
                gr.DrawLine(ro_cellBorderPen, draw_x, .0f, draw_x, pbFieldDisplay.ClientSize.Height);
            }

            //horizontal lines:
            for (int y = 0; y < sc_Game.nFIELD_HEIGHT_CELLS + 1; ++y) {
                float draw_y = y * cell_height_pix;
                gr.DrawLine(ro_cellBorderPen, .0f, draw_y, pbFieldDisplay.ClientSize.Width, draw_y);
            }
#else
            var field_image = sc_LinesResourcesProvider.st_GetFieldBackground().pr_Image!;
            gr.DrawImage(field_image, pbFieldDisplay.ClientRectangle);
#endif
            #endregion

            /* draw balls: */
            #region Draw Balls
            for (int x = 0; x < sc_Game.nFIELD_WIDTH_CELLS; ++x) {
                for (int y = 0; y < sc_Game.nFIELD_HEIGHT_CELLS; ++y) {
                    var ball = sc_Game.st_GetBallAt(x, y);
                    if (ball != null) {
#if PROCEDURAL_DRAWING
                        float size_factor;

                        switch (ball.State) {
                            case en_BallState.eGrowed:      size_factor = 0.8f; break;
                            case en_BallState.eGrowing:
                            case en_BallState.eNotGrowed:   size_factor = 0.35f; break;
                            default: continue; //skip destructing balls
                        }

                        float scaled_width = cell_width_pix * size_factor;
                        float scaled_height = cell_height_pix * size_factor;

                        ro_ballsBrush.Color = st_switchColor(ball.Color);

                        gr.FillEllipse(ro_ballsBrush, x * cell_width_pix + (cell_width_pix - scaled_width) / 2f, y * cell_height_pix + (cell_height_pix - scaled_height) / 2f, scaled_width, scaled_height);
#else
                        Image image;
                        if (ball.CurrentAnim != null) {
                            image = ball.CurrentAnim?.GetCurrentFrame()!;
                        }
                        else {
                            image = sc_LinesResourcesProvider.st_GetBallImage(ball.Color, ball.State == en_BallState.eGrowed);
                        }
                        calculate_ball_drawing_pos(out float draw_x, out float draw_y, stretch_x, stretch_y, x, y);

                        gr.DrawImage(image, draw_x, draw_y, sc_LinesResourcesProvider.fFIELD_CELL_SIZE_PX * stretch_x, sc_LinesResourcesProvider.fFIELD_CELL_SIZE_PX * stretch_y);
#endif
                    }
                }
            }
            #endregion

            /* display ball moving path: */
            #region Display Path
            var path = sc_Game.st_GetBallMovingPath();
            var moving_ball = sc_Game.st_GetMovingBall();

            if (path.Count >= 2 && moving_ball != null) {
                if ((pathPoints?.Length ?? -1) != path.Count) {
                    pathPoints = new PointF[path.Count];
                }

#if !PROCEDURAL_DRAWING
                ro_pathLinePen.Width = fPATH_PEN_WIDTH * (stretch_x + stretch_y) * 0.5f;
#endif

                for (int i = 0; i < path.Count; ++i) {
#if PROCEDURAL_DRAWING
                    pathPoints![i] = new PointF(path[i].m_nX * cell_width_pix + cell_width_pix / 2, path[i].m_nY * cell_height_pix + cell_height_pix / 2);
#else
                    calculate_ball_drawing_pos(out float px, out float py, stretch_x, stretch_y, path[i].m_nX, path[i].m_nY);
                    px += sc_LinesResourcesProvider.fFIELD_CELL_SIZE_PX * 0.5f * stretch_x;
                    py += sc_LinesResourcesProvider.fFIELD_CELL_SIZE_PX * 0.5f * stretch_y;
                    pathPoints![i] = new PointF(px, py);
#endif
                }

                gr.DrawLines(ro_pathLinePen, pathPoints!);
            }
            #endregion

            /* display moving ball: */
            #region Draw Moving Ball
#if PROCEDURAL_DRAWING
            if (moving_ball != null) {
                ro_ballsBrush.Color = st_switchColor(moving_ball.Color);
                gr.FillEllipse(ro_ballsBrush, moving_ball.Pos.m_nX * cell_width_pix, moving_ball.Pos.m_nY * cell_height_pix, cell_width_pix, cell_height_pix);
            }
#else
            if (moving_ball != null) {
                var img = sc_LinesResourcesProvider.st_GetBallImage(moving_ball.Color, isGrowed: true);
                float size_x = sc_LinesResourcesProvider.fFIELD_CELL_SIZE_PX * stretch_x * fMOVING_BALL_SIZE_FACTOR;
                float size_y = sc_LinesResourcesProvider.fFIELD_CELL_SIZE_PX * stretch_y * fMOVING_BALL_SIZE_FACTOR;
                calculate_ball_drawing_pos(out float draw_x, out float draw_y, stretch_x, stretch_y, moving_ball.Pos.m_nX, moving_ball.Pos.m_nY);
                draw_x -= (size_x - sc_LinesResourcesProvider.fFIELD_CELL_SIZE_PX * stretch_x) * 0.5f;  //need to fix drawing position because of the scale
                draw_y -= (size_y - sc_LinesResourcesProvider.fFIELD_CELL_SIZE_PX * stretch_y) * 0.5f;
                gr.DrawImage(img, draw_x, draw_y, size_x, size_y);
            }
#endif
            #endregion

            /* highlight selected ball: */
            #region Highlight Selection
#if PROCEDURAL_DRAWING
            if (sc_Game.st_pr_SelectedBallPos != null && moving_ball == null) {
                var (x, y) = sc_Game.st_pr_SelectedBallPos.Value;
                gr.DrawRectangle(Pens.Yellow, x * cell_width_pix, y * cell_height_pix, cell_width_pix, cell_height_pix);
            }
#endif
            #endregion
        }

        private void pbFieldDisplay_MouseClick(object? sender, MouseEventArgs e)
        {
            calculateCellSizeInPixels(out float cell_width_pix, out float cell_height_pix);

            int x = ( int )(e.X / cell_width_pix);
            int y = ( int )(e.Y / cell_height_pix);

            sc_Game.st_UserSelectCell(x, y);
        }

        private void wndUpdatingTimer_Tick(object? sender, EventArgs e)
        {
            //Update UI:
            Invalidate(invalidateChildren: true);
            Update();

            tsmiGame_CancelLastTurn.Enabled = sc_Game.st_CanCancelLastTurn();

#if PROCEDURAL_DRAWING
            TimeSpan ts = new(0, 0, ( int )sc_Game.st_pr_fGameTimeSeconds);
            lblTime.Text = "Time: " + ts.ToString();

            lblScore.Text = "Score: " + sc_Game.st_pr_unScore.ToString(ro_sScoreFormat);

            uint score = 0U;
            if (sc_Game.st_m_ro_Leaderboard.pr_nEntriesNumber > 0) {
                score = sc_Game.st_m_ro_Leaderboard.GetEntryRRef(idx: 0).m_ro_unScore;
            }
            lblBestScore.Text = "Best score: " + score.ToString(ro_sScoreFormat);
#endif
        }

        private void onGameOver(uint score, float gameTimeSeconds)
        {
            Action message = delegate () {
                TimeSpan ts = new(0, 0, ( int )gameTimeSeconds);
                MessageBox.Show(owner: this, $"Game Over! Score: {score}, time: {ts}", sFORM_HEADER, MessageBoxButtons.OK, MessageBoxIcon.Information);
            };

            Invoke(message);

            sc_Game.st_Restart();
        }

        private void setCorrectClientSize()
        {
            var panel_native_size = ( SizeF )sc_LinesResourcesProvider.st_GetPanelBackground().pr_Image!.Size;
            var field_native_size = ( SizeF )sc_LinesResourcesProvider.st_GetFieldBackground().pr_Image!.Size;

            st_Assert(panel_native_size.Width == field_native_size.Width, $"Resources inconsistency! panel sz: {panel_native_size}, field sz: {field_native_size}");

            float native_width_px = field_native_size.Width;
            float native_height_px = field_native_size.Height + panel_native_size.Height;
            float wid_to_hgt_ratio = native_width_px / ( float )native_height_px;


            /* Setup correct client size with saving window proportions: */
            var client_sz = ClientSize;

            if (clientSizeOnLatestLayout != null) {
                if (clientSizeOnLatestLayout != client_sz) {
                    bool change_height;

                    if (client_sz.Width == clientSizeOnLatestLayout?.Width) {
                        change_height = false;
                    }
                    else if (client_sz.Height == clientSizeOnLatestLayout?.Height) {
                        change_height = true;
                    }
                    else {
                        change_height = client_sz.Width < client_sz.Height;
                    }

                    if (change_height) {
                        client_sz.Height = ( int )((msMenu.Height + 2f * fUI_VERTICAL_PADDING_PX) + client_sz.Width / wid_to_hgt_ratio);
                    }
                    else {
                        client_sz.Width = ( int )((client_sz.Height - msMenu.Height - 2f * fUI_VERTICAL_PADDING_PX) * wid_to_hgt_ratio);
                    }
                }
            }

            ClientSize = client_sz;
        }

        private void calculateCellSizeInPixels(out float width, out float height)
        {
            float area_width_pix = pbFieldDisplay.ClientSize.Width;
            float area_height_pix = pbFieldDisplay.ClientSize.Height;

            width = area_width_pix / sc_Game.nFIELD_WIDTH_CELLS;
            height = area_height_pix / sc_Game.nFIELD_HEIGHT_CELLS;
        }

#if !PROCEDURAL_DRAWING
        private void calculateStretchFactor(out float x, out float y)
        {
            var image_size = sc_LinesResourcesProvider.st_GetFieldBackground().pr_Image!.Size;
            var client_size = ( SizeF )pbFieldDisplay.ClientSize;

            x = client_size.Width / image_size.Width;
            y = client_size.Height / image_size.Height;
        }

        private static void st_drawScore(Size taargetSize, Graphics gr, string score)
        {
            var font = sc_LinesResourcesProvider.st_GetScoreFont();

            float char_height = taargetSize.Height;
            float char_width = (taargetSize.Width - fGAP_BETWEEN_SCORE_CHARS_PX * (nSCORE_CHAR_COUNT - 1)) / ( float )nSCORE_CHAR_COUNT;

            for (int i = 0; i < nSCORE_CHAR_COUNT; ++i) {
                gr.DrawImage(font[score[i]], (char_width + fGAP_BETWEEN_SCORE_CHARS_PX) * i, 0, char_width, char_height);
            }
        }
#endif

#if PROCEDURAL_DRAWING
#if !DEBUG
#pragma warning disable 8524
#endif
        private static Color st_switchColor(en_BallColors color) => color switch {
            en_BallColors.eDarkBlue => Color.DarkBlue,
            en_BallColors.eLightBlue => Color.LightBlue,
            en_BallColors.eLightRed => Color.Red,
            en_BallColors.eDarkRed => Color.DarkRed,
            en_BallColors.eGreen => Color.Green,
            en_BallColors.eYellow => Color.Yellow,
            en_BallColors.ePink => Color.Pink,
#if DEBUG
            _ => throw new Exception(color.ToString())
#endif
        };
#if !DEBUG
#pragma warning restore 8524
#endif
#endif

        private static void st_onTriggerSound(en_LinesSounds sound)
        {
#if DEBUG
            System.Diagnostics.Debug.Print($"Sound: {sound}");
#endif
            sc_LinesSounds.st_GetSound(sound).Play();
        }
    }


    internal static class sc_WinUIUtils
    {
        public static void ext_SetLocation(this Control self, int x, int y)
        {
            var loc = self.Location;
            loc.X = x;
            loc.Y = y;
            self.Location = loc;
        }
        public static void ext_SetClientSize(this Control self, int wid, int hgt)
        {
            var sz = self.ClientSize;
            sz.Width = wid;
            sz.Height = hgt;
            self.ClientSize = sz;
        }
    }
}
